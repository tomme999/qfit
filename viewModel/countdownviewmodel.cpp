#include "countdownviewmodel.h"

CountDownViewModel::CountDownViewModel(QObject *parent) : QObject(parent)
{
    _timer.setInterval(500);
    _timer.setSingleShot(false);
    connect(&_timer,&QTimer::timeout,this,&CountDownViewModel::timerTimeout);
}

CountDownModel *CountDownViewModel::getModel()
{
    return & this->_model;
}

void CountDownViewModel::startTimer()
{
    this->_timer.start();
}

void CountDownViewModel::timerTimeout()
{
    this->setSeconds(this->getSeconds()-1);
}

short CountDownViewModel::getMinutes() const
{
    return _minutes;
}

void CountDownViewModel::setMinutes(short minutes)
{
    if(minutes>=0 && minutes!=_minutes)
    {
        _minutes = minutes;
        this->minutesChanged();
    }
}

short CountDownViewModel::getSeconds() const
{
    return _seconds;
}

void CountDownViewModel::setSeconds(short seconds)
{
    if(seconds>=0 && seconds!=_seconds)
    {
        _seconds = seconds;
        qDebug() << _seconds;
        this->secondsChanged();
    }
}

void CountDownViewModel::initData()
{
    this->setMinutes(this->getModel()->getMinutes());
    this->setSeconds(this->getModel()->getSeconds());
}
