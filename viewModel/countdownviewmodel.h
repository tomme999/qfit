#ifndef COUNTDOWNVIEWMODEL_H
#define COUNTDOWNVIEWMODEL_H

#include <QDebug>
#include <QObject>
#include <QTimer>

#include "models/countdownmodel.h"

class CountDownViewModel : public QObject
{
    Q_OBJECT
public:
    explicit CountDownViewModel(QObject *parent = 0);

    Q_PROPERTY(short minutes MEMBER _minutes READ getMinutes WRITE setMinutes NOTIFY minutesChanged)
    Q_PROPERTY(short seconds MEMBER _seconds READ getSeconds WRITE setSeconds NOTIFY secondsChanged)

    CountDownModel * getModel();

    short getMinutes() const;
    void setMinutes(short getMinutes);

    short getSeconds() const;
    void setSeconds(short seconds);

    void initData();

    Q_INVOKABLE void startTimer();

private:

    CountDownModel _model;
    QTimer _timer;
    short   _minutes;
    short   _seconds;

signals:
    void minutesChanged();
    void secondsChanged();

public slots:
    void timerTimeout();
};

#endif // COUNTDOWNVIEWMODEL_H
