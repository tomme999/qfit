import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked:
        {
            ViewModel.startTimer();
        }

        myText.onTextChanged:
        {
            if(myText.text==="-1")
            {
                Qt.quit();
            }
        }
    }
}
