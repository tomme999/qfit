#ifndef QLINQ2Q_H
#define QLINQ2Q_H

#ifdef QLINQ_H
#error Do not include qlinq.h and qlinq2q.h ! use only qlinq2q.h
#endif


#include <QObject>

#include <type_traits>
#include "qlinq.h"
#include "qlinq2t.h"

//TODO : Remove : TINP.
//TODO : Remove QLists
template <class T> class QLinq2Q : public QLinq<T>
{
    typedef typename T::value_type TI;
    typedef typename std::remove_pointer<TI>::type  TINP;
public:

    QLinq2Q(T &myList) : QLinq<T>(myList) {}

    // TODO anyEquals
    template <typename R> inline bool anyEquals (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()==cmpValue; };
        return this->any(func);
    }

    // TODO anyGreater
    template <typename R> inline bool anyGreater (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()>cmpValue; };
        return this->any(func);
    }

    // TODO anyGreaterOrEquals
    template <typename R> inline bool anyGreaterOrEquals (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()>=cmpValue; };
        return this->any(func);
    }

    // TODO anySmaller
    template <typename R> inline bool anySmaller (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()<cmpValue; };
        return this->any(func);
    }
    // TODO anySmallerOrEquals
    template <typename R> inline bool anySmallerOrEquals (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()<=cmpValue; };
        return this->any(func);
    }

    // TODO allEquals
    template <typename R> inline bool allEquals (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()==cmpValue; };
        return this->all(func);
    }
    // TODO allGreater
    template <typename R> inline bool allGreater (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()>cmpValue; };
        return this->all(func);
    }
    // TODO allGreaterOrEquals
    template <typename R> inline bool allGreaterOrEquals (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()>=cmpValue; };
        return this->all(func);
    }
    // TODO allSmaller
    template <typename R> inline bool allSmaller (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()<cmpValue; };
        return this->all(func);
    }
    // TODO allSmallerOrEquals
    template <typename R> inline bool allSmallerOrEquals (R (TINP::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,val)()<=cmpValue; };
        return this->all(func);
    }

    using QLinq<T>::avg;

    template <typename RF,typename R, class TI2 = typename std::remove_pointer<TI>::type> inline RF avg(R (TI2::* pm)() const) const
    {
//        auto func = (std::is_pointer<TI>::value) ?
//                  [pm](const TI & myItem) { std::function<R()> memberFunction =  std::bind(pm,*myItem); return memberFunction(); }
//                : [pm](const TI & myItem) { std::function<R()> memberFunction =  std::bind(pm,myItem); return memberFunction(); };

        auto func = [pm](const TI & myItem) { std::function<R()> memberFunction =  std::bind(pm,myItem); return memberFunction(); };


        return this->avg<RF>(func);
    }

//    template <class TI2 = typename std::remove_pointer<TI>::type> inline auto avg2(auto a) const
//    {
//        if(std::is_member_object_pointer<decltype(a)>::value)
//        {
//            //R (TI2::*pm)() = a;
//            auto func = (std::is_pointer<TI>::value) ?
//                      [a](const TI & myItem) { auto memberFunction =  std::bind(a,*myItem); return memberFunction(); }
//                    : [a](const TI & myItem) { auto memberFunction =  std::bind(a,myItem); return memberFunction(); };

//            return QLinq<T>::avg(func);
//        }
//    }

    template <typename R> inline R sum(R (TINP::* pm)()) const
    {
        R result = 0;

        auto func = (std::is_pointer<TI>::value) ?
                  [pm](const TI & myItem) { std::function<R()> memberFunction =  std::bind(pm,*myItem); return memberFunction(); }
                : [pm](const TI & myItem) { std::function<R()> memberFunction =  std::bind(pm,myItem); return memberFunction(); };

        return QLinq<T>::sum(func);
    }

    /// \brief cast a list items to another type
    /// \tparam R : result type of an item
    template<class R>
    QLinq2Q<QList<R>> cast()
    {
        QList<R> lst = this->QLinq<T>::_cast();

        QLinq2Q<QList<R>> result (lst);
        return result;
    }

    /// \brief Concatenation of 2 Lists
    /// \param data the objects of the list must be the same of which stored in the active QLinq list
    /// \returns A new QLinq with all the items concatenated
    QLinq2Q<T> concat(const T & data) const
    {
        T tmpData = this->QLinq<T>::_concat(data);
        return QLinq2Q(tmpData);
    }

    //using QLinq<T>::count;

    /// \brief Keeps only one value in the current QLinq List.
    /// \returns A new QLinq with the list cleaned
    /// \remarks TI must implement operator==
    inline QLinq2Q<T> distinct()
    {
        T t = this->_distinct(this->_t);
        return (QLinq2Q<T>(t));
    }

    /// \brief Remove a list of items from the current QLinq List.
    /// \param data A list of items to be remove from the current QLinq List
    /// \remarks TI must implement operator !=
    /// \remarks data must be a container of TI
    QLinq2Q<T> except(const T & data) const
    {
        T tmpData = this->_except_lq(data);
        return QLinq2Q<T>(tmpData);
    }

    /// \brief Loops on all the items
    /// \param func Lambda function to process on one item
    /// \returns returns the active QLinq list
    QLinq2Q<T> forEach(const std::function<void(const TI &)> &func) const
    {
        this->_forEach(func);
        return *this;
    }

    /// \brief Returns a QLinq with the result of a group by
    /// \tparam F Class type of a group line.
    /// \tparam R Class type of the result of the group by.
    /// \param funcFields : returns a line to be grouped.
    /// \param func : returns a line of the result.
    /// \remarks F must implement operator ==
//    template <class F, class R> QLinq2Q<QList<QLinq2Q::R> > groupBy(const std::function<F(const TI &)> funcFields,const std::function<R(const F&,const QLinq2Q<T>&)> &func) const
//    {
//        QList<R> result = this->_groupBy(funcFields,func);
//        return QLinq2Q<QList<R>>(result);
//    }

//    template <class F, class R> QLinq<QList<R>> groupBy(const std::function<F(const TI &)> funcFields,const std::function<R(const F&,const QLinq<T>&)> &func) const = delete;

    // TODO groupJoin returning QLinq2Q

    /// \brief Returns a QLinq of a list of items belonging to the current list and to a second list.
    /// \param data : second list.
    /// Returns QLinq2Q<T>
    QLinq2Q<T> intersect(const T & data) const
    {
        T tmpData = this->_intersect(data);
        return QLinq2Q(tmpData);
    }

//TODO Remove QList
    template <class F, class R, class LJ, class J = typename LJ::value_type> QLinq<QList<R>>
    join(
            const LJ &joinTable
    ,       const std::function<F(const TI&)> &funcPrimary
    ,       const std::function<F(const typename LJ::value_type&)> &funcForeign
    ,       const std::function<R(TI &,typename LJ::value_type &)> &functionSelect
    )
    {
        QList<R> resultList = this->_join(joinTable, funcPrimary, funcForeign, functionSelect);
        return QLinq<QList<R>>(resultList);
    }
//TODO Remove QList
    template <class R, class LJ, class J = typename LJ::value_type> QLinq<QList<R>>
    join(
            const LJ &joinTable
    ,       const std::function<bool(const TI&, const typename LJ::value_type&)> &funcJoin
    ,       const std::function<R(TI &, typename LJ::value_type &)> &functionSelect
    )
    {
        QList<R> resultList = this->_join(joinTable, funcJoin, functionSelect);
        return QLinq<QList<R>>(resultList);
    }

    #if __IS_CPP_14__
    template <class LJ, class J = typename LJ::value_type> QLinq2T<QList<std::tuple<TI, J>>>
    join(
            const LJ &joinTable
    ,       const std::function<bool(const TI&, const typename LJ::value_type&)> &funcJoin
    )
    {
        QList<std::tuple<TI, J>> resultList = this->_join(joinTable, funcJoin);
        return QLinq2T<QList<std::tuple<TI, J>>>(resultList);
    }

    template <class R, class LJ, class J = typename LJ::value_type,typename NPJ= typename std::remove_pointer<J>::type> QLinq2T<QList<std::tuple<TI,J>>>
    join(
            const LJ &joinTable
    ,       R (std::remove_pointer<TI>::type::* pm)() const
    ,       R (NPJ::* fg)() const
    )
    {
        const std::function<bool(const TI&, const J&)> funcJoin = [pm,fg](const TI& a, const J& b)->bool { return std::bind(pm,a)() == std::bind(fg,b)(); };
        return this->join(joinTable,funcJoin);
    }

    #endif

    // TODO orderBy returning QLinq2Q
    template<class T2 = T, class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, QLinq2Q<T>>::type orderBy()
    {
        std::sort(this->_t.begin(), this->_t.end(), [](TI2 t1,TI2 t2) { return (t1<t2); });
        return *this;
    }

    template<class T2 = T, class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, QLinq2Q<T>>::type orderBy()
    {
        std::sort(this->_t.begin(), this->_t.end(), [](TI2 t1,TI2 t2) { return (*t1<*t2); });
        return *this;
    }

    template <class R> QLinq2Q<T> orderBy(const std::function<R(TI)> &func)
    {
        this->_orderBy(func,this->_t);
        return *this;
    }

    template <class R,class TI2=TI>
    bool _orderByPrivate (const TI& elt1, const TI& elt2, R (TI2::* func)() const)
    {
        auto e1 = std::bind(func,elt1)();
        auto e2 = std::bind(func,elt2)();
        if(e1<e2) return true;

        return false;
    }

    template <class R, typename ... Args,class TI2=TI>
    bool _orderByPrivate (const TI& elt1, const TI& elt2, R (TI2::* func)() const, Args ... args)
    {
        auto e1 = std::bind(func,elt1)();
        auto e2 = std::bind(func,elt2)();
        if(e1<e2) return true;
        if(e1>e2) return false;
        if(sizeof...(args)!=0)
        {
            return _orderByPrivate(elt1,elt2,args...);
        }
        return false;
    }

    //template <class R,typename ... Args,class TI2=TI>

//    bool _orderByPrivate (const TI& elt1, const TI& elt2, const std::function<auto(const TI&)> &func, Args ... args)
//    {
//        auto e1 = func(elt1);
//        auto e2 = func(elt2);
//        if(e1<e2) return true;
//        if(e1>e2) return false;
//        if(sizeof...(args)!=0)
//        {
//            return _orderByPrivate(elt1,elt2,args...);
//        }
//        return false;
//    }

//    template <class R,typename ... Args,class TI2=TI>
//    bool _orderByPrivate (const TI& elt1, const TI& elt2, R &func, Args ... args)
//    {
//        auto e1 = func(elt1);
//        auto e2 = func(elt2);
//        if(e1<e2) return true;
//        if(e1>e2) return false;
//        if(sizeof...(args)!=0)
//        {
//            return _orderByPrivate(elt1,elt2,args...);
//        }
//        return false;
//    }

    template <class R, typename ... Args,class TI2=TI>
    QLinq2Q<T> orderBy(R (TI2::* pm)() const, Args ... args)
    {
        // TODO... orderby recursive

        auto func = [&,this,pm,args...](const TI & elt1, const TI & elt2) { return this->_orderByPrivate(elt1,elt2,pm,args...); };
        this->_orderBy(func,this->_t);
        return *this;
    }

    // TODO orderByDescending returning QLinq2Q

    /// \brief Reverse all the items in the current list.
    QLinq2Q<QList<TI>> reverse()
    {
        return QLinq2Q<TI>(QLinq<T>::_reverse());
    }

    //template <class R> QLinq<QList<R>> select(const std::function<R(TI)> &func)
    template <class R>
    typename std::enable_if<std::is_arithmetic<R>::value, QLinq<QList<R>>>::type
    select(const std::function<R(TI)> &func)
    {
        QList<R> result = this->_select(func);
        QLinq<QList<R>> myLinq(result);
        return myLinq;
    }

    template <class R>
    typename std::enable_if<!std::is_arithmetic<R>::value, QLinq2Q<QList<R>>>::type
    select(const std::function<R(TI)> &func)
    {
        QList<R> result = this->_select(func);
        QLinq2Q<QList<R>> myLinq(result);
        return myLinq;
    }

    // TODO comment selectMany returning QLinq2Q
    template <typename R, typename J>
    typename std::enable_if<std::is_arithmetic<R>::value, QLinq<QList<R>>>::type
    selectMany(J joinTable, const std::function<R(const TI &,const typename J::value_type &)> &functionSelect)
    {
        QList<R> lstResult = this->_selectMany(joinTable,functionSelect);
        return QLinq<QList<R>>(lstResult);
    }


    template <typename R, typename J>
    typename std::enable_if<!std::is_arithmetic<R>::value, QLinq2Q<QList<R>>>::type
    selectMany(J joinTable, const std::function<R(const TI &,const typename J::value_type &)> &functionSelect)
    {
        QList<R> lstResult = this->_selectMany(joinTable,functionSelect);
        return QLinq2Q<QList<R>>(lstResult);
    }

    template <typename J,class R=std::tuple<TI,typename J::value_type>>
    QLinq2T<QList<std::tuple<TI,typename J::value_type>>>
    selectMany(J joinTable)
    {
        const std::function<R(const TI &,const typename J::value_type &)> &functionSelect
                = [](const TI& a,const typename J::value_type & b)->R { return std::make_tuple(a,b); };

        QList<R> lstResult = this->_selectMany(joinTable,functionSelect);
        return QLinq2T<QList<R>>(lstResult);
    }

    using QLinq<T>::groupBy;

    //! \brief Group data according to a property of TI
    //! \tparam F type of the data for the grouping.
    //! \tparam R type of data of the result of one line of group by.
    //! \param pm is a reference to a member function (or variable) of TI. This method must be an accessor and must return a 'F' type.
    //! the grouping will be made with this property.
    //! \param func, is a lambda function, called foreach group calculated.
    template <class F, class R> QLinq2Q<QList<R>> groupBy(F (std::remove_pointer<TI>::type::* pm)() const,const std::function<R(const F&,QLinq2Q<T>)> &func) const
    {
        // Build the dictionnary
        QHash<F,QList<TI>> dictionnary;
        foreach(auto item, this->_t)
        {
            std::function<F()> memberFunction =  std::bind(pm,item);

            F key = memberFunction();
            if(dictionnary.contains(key))
            {
                dictionnary[key].append(item);
            }
            else
            {
                dictionnary[key] = QList<TI>();
                dictionnary[key].append(item);
            }
        }

        // Execute the statements;
        QList<R> result;
        foreach(auto key, dictionnary.keys())
        {
            result.append( func(key, QLinq2Q(dictionnary[key])) );
        }
        return QLinq2Q<QList<R>>(result);
    }

    //using QLinq<T>::select;

//    template <typename R> QLinq<R> select(R(TI::* pm)() const) const
//    {
//        QList<R> lst;
//        foreach(TI item,this->_t)
//        {
//            QObject & qoitem = dynamic_cast<QObject &>(item);
//            std::function<R()> memberFunction =  std::bind(pm,item);
//            lst.append(memberFunction());
//        }
//        return from(lst);
//    }

    template <typename R> QLinq<R> select() const
    {
        QList<R> lst;
        foreach(TI item,this->_t)
        {
            R r;

            lst.append(r);
        }
        return from(lst);
    }

    using QLinq<T>::sum;

    template <typename O>
    inline O sum(O (TINP::* pm)() const) const
    {
        O result = 0;
        foreach(TI item,this->_t)
        {
            std::function<O()> memberFunction =  std::bind(pm,item);
            result += memberFunction();
        }
        return result;
    }

    using QLinq<T>::max;

//    template <typename R,class TI2 = TI>
//    typename std::enable_if<!std::is_pointer<TI2>::value, R>::type
//    max(R (TINP::* pm)()) const
//    {
//        return this->max<R>([pm] (const TI& val)
//        {
//            std::function<R()> memberFunction =  std::bind(pm,val);
//            return memberFunction();
//        });
//    }

//    template <typename R,class TI2 = TI>
//    typename std::enable_if<std::is_pointer<TI2>::value, R>::type
//    max(R (TINP::* pm)()) const
//    {
//        return this->max<R>([pm] (const TI& val)
//        {
//            std::function<R()> memberFunction =  std::bind(pm,*val);
//            return memberFunction();
//        });
//    }

    template <typename R>
    inline R max(R (TINP::* pm)() const) const
    {
        return this->max([pm] (const TI& val)
        {
            std::function<R()> memberFunction =  std::bind(pm,val);
            return memberFunction();
        });
    }



    using QLinq<T>::maxItem;

    template <typename R,class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, TI2>::type
    maxItem(R (TINP::* pm)()) const
    {
        return this->maxItem<R>([pm] (const TI& val)
        {
            std::function<R()> memberFunction =  std::bind(pm,val);
            return memberFunction();
        });
    }

    template <typename R,class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, TI2>::type
    maxItem(R (TINP::* pm)()) const
    {
        return this->maxItem<R>([pm] (const TI& val)
        {
            std::function<R()> memberFunction =  std::bind(pm,*val);
            return memberFunction();
        });
    }

    using QLinq<T>::min;

    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if<std::is_member_pointer<LF>::value, typename std::remove_reference<R>::type>::type
    min(const LF) const
    { return 0 ; }

    template <typename R>
    //inline R min(R (TINP::* pm)() const) const
    inline R min(R (TINP::* pm)() const) const
    {
        return this->min([pm] (const TI& val)
        {
            std::function<R()> memberFunction =  std::bind(pm,val);
            return memberFunction();
        });
    }

    using QLinq<T>::minItem;

//    template <typename R,class TI2 = TI>
//    typename std::enable_if<!std::is_pointer<TI2>::value, TI2>::type
//    minItem(R (TINP::* pm)()) const
//    {
//        return this->minItem<R>([pm] (const TI& val)
//        {
//            std::function<R()> memberFunction =  std::bind(pm,val);
//            return memberFunction();
//        });
//    }

//    template <typename R,class TI2 = TI>
//    typename std::enable_if<std::is_pointer<TI2>::value, TI2>::type
//    minItem(R (TINP::* pm)()) const
//    {
//        return this->minItem<R>([pm] (const TI& val)
//        {
//            std::function<R()> memberFunction =  std::bind(pm,*val);
//            return memberFunction();
//        });
//    }

    template <typename R>
    TI minItem(R (TINP::* pm)() const) const
    {
        return this->minItem<R>([pm] (const TI& val)
        {
            std::function<R()> memberFunction =  std::bind(pm,val);
            return memberFunction();
        });
    }

    template <class V> QLinq2Q<T> recursive
    (
        V(TINP::* primary)() const
    ,   V(TINP::* foreign)() const
    ,   const std::function<bool(const TI &)> &funcWherePrimary
    ,   const std::function<bool(int level, const TI &previous, const TI &actual)> &funcWhereForeign = nullptr
    ,   int levelEnd = -1
    )
    {
        T result;
        QHash<V,QList<TI>> dictionnary;
        bool indexCreated = false;
        int level = 0;
        // First iteration
        foreach(auto item, this->_t)
        {
            if(funcWherePrimary(item))
            {
                // Create an index
                if(!indexCreated)
                {
                    indexCreated = true;
                    foreach(auto itemChild, this->_t)
                    {
                        std::function<V()> memberFunction =  std::bind(foreign,itemChild);
                        V result = memberFunction();
                        if(!dictionnary.contains(result))
                        {
                            dictionnary[result] = QList<TI>();
                        }
                        dictionnary[result].append(itemChild);
                    }
                }

                if(levelEnd==-1 || level==levelEnd)
                {
                    result.append(item);
                }
                if(level!=levelEnd)
                {
                    _recursive( item, primary, funcWhereForeign, level + 1, dictionnary,result,levelEnd);
                }
            }
        }
        QLinq2Q<T> myLinq(result);
        return myLinq;
    }

    //! \brief this version of recursive allow to set the first query with PrimaryKey = PrimaryValue
    template <class V> QLinq2Q<T> recursive
    (
        V(TINP::* primary)() const
    ,   V(TINP::* foreign)() const
    ,   V const &primaryValue
    ,   const std::function<bool(int level, const TI &previous, const TI &actual)> &funcWhereForeign = nullptr
    ,   int levelEnd = -1
    )
    {
        return recursive<V>
        (
            primary
        ,   foreign
        ,   [primary, primaryValue](const TI & primaryItem)
        {
            std::function<V()> memberFunction =  std::bind(primary,primaryItem);
            V result = memberFunction();
            return result == primaryValue;
        }
        ,   funcWhereForeign
        ,   levelEnd
        );
    }

    // TODO Skip Comment
    QLinq2Q<T> skip(int nb)
    {
        T lst = this->_skip((nb));
        return QLinq2Q(lst);
    }

    // TODO SkipWhile Comment
    QLinq2Q<T> skipWhile(const std::function<bool(const TI &)> &func)
    {
        T lst = this->_skipWhile(func);
        return QLinq2Q(lst);
    }

    /// \brief get the first items from a list.
    /// \param nb : number of items to take from the list
    /// \return the first items asked.
    ///  \remarks if nb is greater than the number of item, the full list will be returned
    QLinq2Q<T> take(int nb)
    {
        if(nb>=this->count()) return *this;

        T lst = this->_take(nb);
        return QLinq2Q(lst);
    }

    /// \brief get the first items from a list while the condition is true.
    /// \param funct : condition tested on all items until it returns false. func has one arguement of the type of the item of the list and returns a boolean.
    /// \return the first items asked.
    QLinq2Q<T> takeWhile(const std::function<bool(const TI &)> &func)
    {
        T lst = this->_takeWhile(func);
        return QLinq2Q(lst);
    }

    // TODO Comment
    QLinq2Q<T> unionJoin(const T & data) const
    {
        T tmpData = this->_unionJoin(data);
        return QLinq2Q(tmpData);
    }

    // TODO comment where returning QLinq2Q
    inline QLinq2Q<T> where(const std::function<bool(const TI &)> &func)
    {
        QList<TI> myList = this->_where(func);
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    // TODO comment whereIf returning QLinq2Q
    inline QLinq2Q<T> whereIf(bool condition, const std::function<bool(const TI &)> &func)
    {
        if(condition)
            return where(func);
        else
            return *this;
    }



    template <typename V> inline QLinq2Q<T> whereEquals(V(TINP::* pm)() const,const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()==val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    template <typename V> inline QLinq2Q<T> whereEquals(V(TINP::* pm)(),const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()==val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    template <typename V> inline QLinq2Q<T> whereLess(V(TINP::* pm)(),const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()<val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    template <typename V> inline QLinq2Q<T> whereLessOrEquals(V(TINP::* pm)(),const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()<=val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    template <typename V> inline QLinq2Q<T> whereGreater(V(TINP::* pm)(),const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()>val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    template <typename V> inline QLinq2Q<T> whereGreater(V(TINP::* pm)() const,const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()>val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    template <typename V> inline QLinq2Q<T> whereGreaterOrEquals(V(TINP::* pm)(),const V &val)
    {
        QList<TI> myList;
        foreach(TI item,this->_t)
        {
            std::function<V()> memberFunction =  std::bind(pm,item);
            if(memberFunction()>=val) { myList.append(item); }
        }
        QLinq2Q<T> myLinq(myList);
        return myLinq;
    }

    /// \brief Associaltes items from a List with another One.
    /// \param lst : second list with items to associate
    /// \param funct : lambda function to associate an item from the current list with a item from lst.
    /// \remarks if lst has more items them the active list, these items will be lost.
    /// \remarks if the current list has more items than lst, these items will be lost.
    /// \return The result will be a QLinq with the result of func on all items.
    template<class R, class O, typename OI = typename O::value_type >
    QLinq2Q<QList<R>> zip(const O &lst,const std::function<R(const TI &elt1, const typename O::value_type &elt2)> &func)
    {
        auto result = _zip(lst, func);
        return QLinq2Q<QList<R>>(result);
    }

protected:
    template <class V> void _recursive
    (
         const TI  &item
    ,    V(TINP::* primary)() const
    ,   const std::function<bool(int level, const TI &previous, const TI &actual)> &funcWhereForeign
    ,   int level
    ,   const QHash<V,QList<TI>> &dictionnary
    ,   T &result
    ,   int levelEnd = -1
    )
    {
        std::function<V()> memberFunction =  std::bind(primary,item);
        V primaryKey = memberFunction();

        foreach(auto itemChild, dictionnary[primaryKey])
        {
            if(funcWhereForeign==nullptr || funcWhereForeign(level,item,itemChild))
            {
                if(levelEnd==-1 || levelEnd==level)
                {
                    result.append(itemChild);
                }
                if(level!=levelEnd)
                {
                    _recursive( itemChild, primary, funcWhereForeign, level + 1, dictionnary, result, levelEnd);
                }
            }
        }
    }
};

//template <class T, class TI = typename T::value_type>
//typename std::enable_if<!std::is_arithmetic<TI>::value && !std::is_pointer<TI>::value, QLinq2Q<T> >::type from(T & myList)
//{
//    QLinq2Q<T> myQLinq(myList);
//    return myQLinq;
//}

//template <class T, class TI = typename T::value_type>
//typename std::enable_if<std::is_arithmetic<TI>::value || std::is_pointer<TI>::value, QLinq<T> >::type from(T & myList)
//{
//    QLinq<T> myQLinq(myList);
//    return myQLinq;
//}
template <class T, class TI = typename T::value_type, class TINP = typename std::remove_pointer<TI>::type>
typename std::enable_if<!std::is_arithmetic<TINP>::value, QLinq2Q<T> >::type from(T & myList)
{
    QLinq2Q<T> myQLinq(myList);
    return myQLinq;
}

template <class T, class TI = typename T::value_type, class TINP = typename std::remove_pointer<TI>::type>
typename std::enable_if<std::is_arithmetic<TINP>::value, QLinq<T> >::type from(T & myList)
{
    QLinq<T> myQLinq(myList);
    return myQLinq;
}

template <class T> QLinq2Q<T> from2q(T & myList)
{
    QLinq2Q<T> myQLinq(myList);
    return myQLinq;
}
#endif // QLINQ2Q_H
