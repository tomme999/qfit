#ifndef QLINQ2T_H
#define QLINQ2T_H
#include <QDebug>
#include <tuple>

/// \brief Get Index of a type in a tuple
namespace qLinq
{
    template <class T, class Tuple>
    struct Index;

    template <class T, class... Types>
    struct Index<T, std::tuple<T, Types...>> {
        static const std::size_t value = 0;
    };

    template <class T, class... Types>
    struct Index<T, std::tuple<T*, Types...>> {
        static const std::size_t value = 0;
    };

    template <class T, class U, class... Types>
    struct Index<T, std::tuple<U, Types...>> {
        static const std::size_t value = 1 + Index<T, std::tuple<Types...>>::value;
    };
}

#if __IS_CPP_14__
template <class T> class QLinq2Q ;

template <class T> class QLinq2T : public QLinq2Q<T>
{
    typedef typename T::value_type TI;
    typedef typename std::remove_pointer<TI>::type  TINP;
// TODO remove QList from all calls
private:


public:

    QLinq2T(T &myList) : QLinq2Q<T>(myList) {}

    // TODO anyEquals
    template <std::size_t I,typename R, typename C> inline bool anyEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()==cmpValue; };
        return this->any(func);
    }

    // TODO anyEquals
    template <typename R, typename C> inline bool anyEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->anyEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO anyGreater
    template <std::size_t I,typename R, typename C> inline bool anyGreater (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()>cmpValue; };
        return this->any(func);
    }

    // TODO anyGreater
    template <typename R, typename C> inline bool anyGreater (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->anyGreater<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO anyGreaterOrEquals
    template <std::size_t I,typename R, typename C> inline bool anyGreaterOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()>=cmpValue; };
        return this->any(func);
    }

    // TODO anyGreaterOrEquals
    template <typename R, typename C> inline bool anyGreaterOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->anyGreaterOrEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO anySmaller
    template <std::size_t I,typename R, typename C> inline bool anySmaller (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()<cmpValue; };
        return this->any(func);
    }

    // TODO anySmaller
    template <typename R, typename C> inline bool anySmaller (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->anySmaller<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO anySmallerOrEquals
    template <std::size_t I,typename R, typename C> inline bool anySmallerOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()<=cmpValue; };
        return this->any(func);
    }

    // TODO anySmallerOrEquals
    template <typename R, typename C> inline bool anySmallerOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->anySmallerOrEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO allEquals
    template <std::size_t I,typename R, typename C> inline bool allEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()==cmpValue; };
        return this->all(func);
    }

    // TODO allEquals
    template <typename R, typename C> inline bool allEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->allEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO allGreater
    template <std::size_t I,typename R, typename C> inline bool allGreater (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()>cmpValue; };
        return this->all(func);
    }

    // TODO allGreater
    template <typename R, typename C> inline bool allGreater (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->allEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO allGreaterOrEquals
    template <std::size_t I,typename R, typename C> inline bool allGreaterOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()>=cmpValue; };
        return this->all(func);
    }

    // TODO allGreaterOrEquals
    template <typename R, typename C> inline bool allGreaterOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->allGreaterOrEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO allSmaller
    template <std::size_t I,typename R, typename C> inline bool allSmaller (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()<cmpValue; };
        return this->all(func);
    }

    // TODO allSmaller
    template <typename R, typename C> inline bool allSmaller (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->allSmaller<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    // TODO allSmallerOrEquals
    template <std::size_t I,typename R, typename C> inline bool allSmallerOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        std::function<bool(const TI &)> func = [cmpValue,pm](const TI & val) { return std::bind(pm,std::get<I>(val))()<=cmpValue; };
        return this->all(func);
    }

    // TODO allSmallerOrEquals
    template <typename R, typename C> inline bool allSmallerOrEquals (R (C::* pm)() const, const R & cmpValue) const
    {
        return this->allSmallerOrEquals<qLinq::Index<C, TI>::value>(pm,cmpValue);
    }

    using QLinq<T>::avg;

    template <std::size_t I,typename R,class C> inline R avg(R (C::* pm)() const) const
    {
        R result = 0;
        foreach(TI item,this->_t)
        {
            std::function<R()> memberFunction =  std::bind(pm,std::get<I>(item));
            result += memberFunction();
        }
        R calc = (this->_t.count()==0)?0:result/this->_t.count();
        return calc;
    }

    template <typename R,class C> inline R avg(R (C::* pm)() const) const
    {
        return this->avg<qLinq::Index<C, TI>::value>(pm);
    }

    using QLinq<T>::min;

    // TODO Comment
    // TODO Test
    template <std::size_t I,typename R,class C> inline R min(R (C::* pm)() const) const
    {
        std::function<R(const TI &)> lambda = [pm](const TI & elt) { std::function<R()> memberFunction =  std::bind(pm,std::get<I>(elt)); return memberFunction(); };

        return this->QLinq<T>::min(lambda);
    }

    // TODO Comment
    // TODO Test
    template <typename R,class C>
    inline R min(R (C::* pm)() const) const
    {
        return this->min<qLinq::Index<C, TI>::value>(pm);;
    }

    using QLinq<T>::max;

    // TODO Comment
    // TODO Test
    template <std::size_t I,typename R,class C>
    inline R max(R (C::* pm)() const) const
    {
        std::function<R(const TI &)> lambda = [pm](const TI & elt) { std::function<R()> memberFunction =  std::bind(pm,std::get<I>(elt)); return memberFunction(); };

        return this->QLinq<T>::max(lambda);
    }

    // TODO Comment
    // TODO Test
    template <typename R,class C>
    inline R max(R (C::* pm)() const) const
    {
        return this->max<qLinq::Index<C, TI>::value>(pm);;
    }

    using QLinq<T>::sum;

    // TODO Comment
    // TODO Test
    template <std::size_t I,typename R,class C> inline R sum(R (C::* pm)() const) const
    {
        std::function<R(const TI &)> lambda = [pm](const TI & elt) { std::function<R()> memberFunction =  std::bind(pm,std::get<I>(elt)); return memberFunction(); };

        return this->QLinq<T>::sum(lambda);
    }

    // TODO Comment
    // TODO Test
    template <typename R,class C> inline R sum(R (C::* pm)() const) const
    {
        return this->sum<qLinq::Index<C, TI>::value>(pm);;
    }

    template <class LJ,class J = typename LJ::value_type >
    auto
    join(
            const LJ &joinTable
    ,       const std::function<bool(const TI&, const typename LJ::value_type&)> &funcJoin) const
    {
        typedef decltype (std::tuple_cat(this->_t.at(0),std::make_tuple(joinTable.at(0)))) tuple_cat_type;
        QList<tuple_cat_type> resultList = this->_joinTuple(joinTable, funcJoin);
        return QLinq2T<QList<tuple_cat_type>>(resultList);
    }

    template <class J, class F, class TPL >
    auto
    join(
            const QList<J> &joinTable
    ,       const std::function<F(const J &)> &funcForeign
    ,       const std::function<F(const TPL&)> &funcPrimary)
    const
    {
        typedef decltype (std::tuple_cat(this->_t.at(0),std::make_tuple(joinTable.at(0)))) tuple_cat_type;
        QList<tuple_cat_type> resultList = this->_joinTuple(joinTable, funcForeign, funcPrimary );
        return QLinq2T<QList<tuple_cat_type>>(resultList);
    }

    template <class TA, class LJ, class R,class J = typename LJ::value_type>
    auto
    join(
            const LJ &joinTable
            ,       R (TA::* pm)() const
            ,       R (J::* fg)() const)
    const
    {
        const std::function<bool(const TI&, const J&)> funcJoin = [pm,fg](const TI& a, const J& b)->bool { return std::bind(pm,std::get<TA>(a))() == std::bind(fg,b)(); };
        return this->join(joinTable,funcJoin);
    }


    template <std::size_t I, class TA, class LJ, class R,class J = typename LJ::value_type>
    auto
    join(
            const typename LJ::value_type &joinTable
            ,       R (TA::* pm)() const
            ,       R (J::* fg)() const)
    const
    {
        const std::function<bool(const TI&, const J&)> funcJoin = [pm,fg](const TI& a, const J& b)->bool { return std::bind(pm,std::get<I>(a))() == std::bind(fg,b)(); };
        return this->join<J>(joinTable,funcJoin);
    }
};
#endif
#endif // QLINQ2T_H

