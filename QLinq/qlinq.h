#ifndef QLINQ_H
#define QLINQ_H

#include <algorithm>
#include <functional>
#include <QDebug>
#include <QMap>
#include <QObject>
#include <QList>
#include <QHash>
#include <type_traits>
#include <typeinfo>
#include <tuple>

#define __QLINQ_VER__ "0.9.1 - Alpha Release - For QFit"
static bool _qlinq_version_shown(false);

#ifndef __IS_CPP_14__
#define __IS_CPP_14__ (__cplusplus >= 201300L)
#endif

/// \brief This abstrract class allow to join 2 classes.
/// \tparam A first class to join
/// \tparam B second class to join
/// \tparam K type of the key used to join both classes
/// \tparam R type of the join result
template <class A, class B, class K, class R> class IQLinqJoiner
{
public:
    IQLinqJoiner () = 0;

    /// \brief Method to compare class A and B.
    /// param a first instance to compare.
    /// param b second instance to compare.
    virtual bool compare(const A &a, const B &b) = 0;

    /// \brief Join method of instances of A and B.
    /// param a first instance to join
    /// param b second instance to join
    /// return join of instance A and B
    virtual R join(const A &a, const B &b ) = 0;

    virtual K getKeyA(const A &a) = 0;
    virtual K getKeyB(const B &b) = 0;

    virtual bool isFullScan() = 0;
};

template <class T> class QLinqCore
{
protected:
    typedef typename T::value_type TI;
};

template <class T> class QLinq : protected QLinqCore<T>
{
    typedef typename T::value_type TI;
    template <class A> using QLinqList = QList<A>;

/// Template Doc
/// \brief
/// \param x - type :
/// \param x - type :
/// \return type :
/// \tparam T - optional/autodeducted - Type Container Class (QList, ...)
/// \tparam TI - optional/autodeducted : Type of items in Container List
/// \tparam R : xxx
/// \remarks This method is active only if...
/// \remarks TI Must implement operator ...
/// \remarks TI must have a default constructor
/// \code
/// ...
/// \endcode
/// \todo Add comment
/// \todo Add unit tests
/// \todo Remove QList in signature
/// \todo Add autodeduction template args
/// \todo replace QList with QLinqList

public:

    /// \brief Constructor of a QLinq Item.
    /// \param myList - container : collection of items to be processed in the QLinq object.
    /// \tparam T - optional/autodeducted : Type Container Class (QList, ...)
    QLinq(T &myList) : _t(myList)
    {
        if(!_qlinq_version_shown)
        {
            qDebug() << "QLinq v"<< __QLINQ_VER__;
            _qlinq_version_shown = true;
        }

    }

    template<typename R> R
    aggregate(const R & initValue,const std::function<R(const R &, const TI &)> &func) const
    {
        /// \brief Calculate a value from all the data in the current list.
        /// \param initValue const R & :
        /// Initial value for the first call of the lambda function
        /// \param func const std::function<R(const R &, const TI &)> & : Lambda function called for each value of the list. The arguments are
        /// result of the previous call (initValue at the first call), and the current item of the list.
        /// \return R : result of the aggregation.
        /// \tparam R - mandatory : Type of the result,
        /// \tparam TI - optional/autodeducted : Type of items in container List.
        /// \todo Add unit tests for aggregate
        /// \todo Add autodeduction template args for aggregate

        R r = initValue;
        foreach(TI item, _t)
        {
            r = func(r,item);
        }
        return r;
    }

    template<typename R> typename std::enable_if<std::is_arithmetic<R>::value, R >::type
    aggregate(const std::function<R(const R &, const TI &)> &func) const
    {
        /// \brief Calculate a value from all the data in the current list.
        /// \param func const std::function<R(const R &, const TI &)> & : Lambda function called for each value of the list. The arguments are
        /// result of the previous call (0 at the first call), and the current item of the list.
        /// \tparam R mandatory : Type of the result
        /// \tparam TI optional/autodeducted : Type of items in container List
        /// \remarks This method is active only if R is arithmetic.
        /// \code
        /// QList<int> lst1;
        /// int init1 = 10;
        /// for(int i=0;i<10; i++) { lst1.insert(lst1.end(), i); }
        /// auto test1 = from(lst1).aggregate<int>(init1, [](auto r, auto i) { return r + i; });
        /// \endcode
        /// \todo Add unit tests for aggregate
        /// \todo Add autodeduction template args for aggregate

        R r = 0;
        return aggregate(r,func);
    }

    template<typename R> typename std::enable_if<!std::is_arithmetic<R>::value, R >::type
    aggregate(const std::function<R(const R &, const TI &)> &func) const
    {
        /// \brief Calculate a value from all the data in the current list.
        /// \param func - const std::function<R(const R &, const TI &)> & : Lambda function called for each value of the list. The arguments are
        /// result of the previous call (default constructor of R will be called at the first call),
        /// and the current item of the list.
        /// \tparam R : Type of the result.
        /// \remarks This method is active only if R is NOT arithmetic.
        /// \remarks R Must have a default contructor.
        /// \todo Add unit tests for aggregate
        /// \todo Add autodeduction template args for aggregate

        R r;
        return aggregate(r,func);
    }

    inline bool all(const std::function<bool(const TI &)> &func) const
    {
        /// \brief Returns true if all the items satifying a condition.
        /// \param func const std::function<bool(const TI &)> & : Lambda function called for each values in the active list.
        /// This lambda must return a boolean.
        /// \return bool : returns true if all the calls of the lambda function returned true.
        /// \tparam T - optional/autodeducted - Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \remarks All won't execute the lambda function on all items of the active list. it will stop if the function returns false.
        /// \code
        /// QList<int> lst;
        /// for (int i=0; i<10; i++) { lst.insert(lst.end(),i); }
        /// auto result = from(lst).all([](const int &i) { return i>=0; });
        /// \endcode

        foreach(TI item, _t)
        {
            if(!func(item))
            {
                return false;
            }
        }
        return true;
    }

    bool any(const std::function<bool(const TI &)> &func) const
    {
        /// \brief Returns true if at least one item of the list matches the lambda function.
        /// \param func const std::function<bool(const TI &)> : Lambda function returning a bool if the item match a condition.
        /// \return bool : returns false if all the items don't match the lambda function.
        /// \tparam T - optional/autodeducted - Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \remarks All won't execute the lambda function on all items of the active list. it will stop if the function returns true.
        /// \code
        /// QList<int> lst;
        /// for (int i=0; i<10; i++) { lst.insert(lst.end(),i); }
        /// auto result = from(lst).any([](const int &i) { return i==3; });
        /// \endcode

        foreach(TI item, _t)
        {
            if(func(item))
            {
                return true;
            }
        }
        return false;
    }

    TI operator[] (int index) const
    {
        /// \brief Gives an item at the selected position.
        /// \param index int : Index from the item in the current list.
        /// \return TI : returns the selected item.
        /// \tparam T - optional/autodeducted - Type Container Class (QList, ...).
        /// \tparam TI - optional/autodeducted : Type of items in Container List.
        /// \code
        /// QList<int> lst;
        /// for (int i=0; i<10; i++) { lst.insert(lst.end(),i); }
        /// auto result = from(lst)[2];
        /// \endcode

        auto iter = this->_t.begin();
        iter += index;
        return *iter;
    }

    TI at(int index) const
    {
        /// \brief Gives an item at the selected position.
        /// \param index int : Index from the item in the current list.
        /// \return TI : returns the selected item.
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...).
        /// \tparam TI - optional/autodeducted : Type of items in Container List.
        /// \remarks at() gives the same item than [] operator.
        /// \code
        /// QList<int> lst;
        /// for (int i=0; i<10; i++) { lst.insert(lst.end(),i); }
        /// auto result = from(lst).at(2);
        /// \endcode

        return (*this)[index];
    }

    template <class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value && std::is_arithmetic<TI2>::value, TI2 >::type
    atOrDefault(int index) const
    {
        /// \brief Gives an item at the selected position. If the selected position do not exists, 0 will be returned.
        /// \param index int : Position of the selected item.
        /// \return TI : returns the selected item, or 0 if the item is not found.
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \remarks This method is active only if TI is not a pointer and TI is arithmetic.
        /// \todo Add unit tests atOrDefault

        if(index < this->_t.length() && index >= 0)
            return (*this)[index];
        else return 0;
    }

    template <class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value && !std::is_arithmetic<TI2>::value, TI2 >::type
    atOrDefault(int index) const
    {
        /// \brief Gives an item at the selected position. If the selected position do not exists, a new instance of TI will be returned.
        /// \param index int : Position of the selected item.
        /// \return TI : returns the selected item, or 0 if the item is not found.
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \remarks This method is active only if TI is not a pointer and TI is not arithmetic.
        /// \remarks TI Must have a default contructor.
        /// \todo Add unit tests atOrDefault

        if(index < this->_t.length() && index >= 0)
            return (*this)[index];
        else return TI();
    }

    template <class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, TI2 >::type
    atOrDefault(int index) const
    {
        /// \brief Gives an item at the selected position. If the selected position do not exists, nullptr will be returned.
        /// \param index int : Position of the selected item.
        /// \return auto : returns the selected item, or 0 if the item is not found.
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \remarks This method is active only if TI is a pointer.
        /// \remarks TI Must implement operator ...
        /// \remarks TI must have a default constructor
        /// \remarks This method is active only if TI is a pointer.
        /// \todo Add unit tests atOrDefault

        if(index < this->_t.length() && index >= 0)
            return (*this)[index];
        else return nullptr;
    }

    template <class TI2 = TI, typename std::enable_if<!std::is_pointer<TI2>::value && std::is_arithmetic<TI2>::value>::type* = nullptr >
    auto
    avg() const
    {
        /// \brief avg calculate the average of all the values in the list
        /// \return auto : the average of all the values in the list. if the list is empty avg will return 0. Please use count() before using avg...
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...).
        /// \tparam TI - optional/autodeducted : Type of items in Container List.
        /// \remarks This method is active only if TI is not a pointer and TI is arithmetic.
        /// \code
        /// QList<int> lst;
        /// for (int i=0; i<10; i++) { lst.insert(lst.end(),i); }
        /// auto result =from(lst).avg();
        /// \endcode

        TI2 result = 0;
        long double count = _t.size();
        foreach(typename T::value_type item, _t)
        {
            result += item;
        }
        return (_t.size()==0)?0:result/count;
    }

    template <class TI2 = TI, typename std::enable_if<!std::is_pointer<TI2>::value && !std::is_arithmetic<TI2>::value>::type* = nullptr>
    auto
    avg() const
    {
        /// \brief avg calculate the average of all the values in the list
        /// \return auto : the average of all the values in the list. if the list is empty avg will return TI(). Please use count() before using avg...
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...).
        /// \tparam TI - optional/autodeducted : Type of items in Container List.
        /// \remarks This method is active only if TI is not a pointer and TI is not arithmetic.
        /// \remarks TI Must implement operator /(long double) and operator +=...
        /// \remarks TI Must have a default constructor.
        /// \code
        /// QList<Complex> lst;
        /// for (int i=0; i<10; i++) { lst.append( Complex(i,2*i)); }
        /// Complex result = from(lst).avg();
        /// \endcode

        TI2 result;
        long double count = _t.size();
        if(_t.count()!=0)
        {
            foreach(typename T::value_type item, _t)
            {
                result += item;
            }
            return result/count;
        }
        return result/++count;
    }

    template <class TI2 = TI, typename std::enable_if<std::is_pointer<TI2>::value>::type* = nullptr >
    auto
    avg() const
    {
        /// \brief avg calculate the average of all the values in the list
        /// \return TI : the average of all the values in the list. if the list is empty avg will return nullptr. Please use count() before using avg...
        /// \tparam T - optional/autodeducted : Type Container Class (QList, ...).
        /// \tparam TI - optional/autodeducted : Type of items in Container List.
        /// \remarks This method is active only if TI is a pointer.
        /// \remarks *TI Must implement operator /(long double) and operator +=...
        /// \remarks *TI Must have a default constructor.
        /// \remarks If the list is empty, avg will return nullptr.
        /// \remarks The result must be destroyed !
        /// \code
        /// QList<Complex *> lst;
        /// for (int i=0; i<10; i++) { lst.append(new Complex(i,2*i)); }
        /// Complex* result = from(lst).avg();
        /// \endcode
        typedef typename std::remove_pointer<TI2>::type TI2Class;
        TI2 result = nullptr;
        long double count = _t.size();
        if(_t.count()!=0)
        {
            result = new TI2Class ();
            foreach(typename T::value_type item, _t)
            {
                *result += *item;
            }

            *result = *result/count;
            return result;
        }
        return (TI2) nullptr;
    }

    /// \brief
    /// \param func Lambda function returning a R type. The average will be calculated from all the results.
    /// \returns A R Type average of all the results of call of the the lambda function.
    template<class R>
    typename std::enable_if<!std::is_pointer<R>::value && std::is_arithmetic<R>::value, R >::type
    avg(const std::function<R(const TI &)> &func) const
    {
        /// \brief avg calculate the average of all the expression given by a lambda function
        /// \param func - const std::function<R(const TI &)> : lambda function returning a value. The average will be calculated from this result.
        /// \return R : Average of all the results of call of the the lambda function.
        /// \tparam T - optional/autodeducted - Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \tparam R : type of the value returned by func, and by the calculation of the average.
        /// \remarks This method is active only if R is not a pointer and is arithmetic.
        /// \remarks R Must implement operator + and /
        /// \code
        /// double result(0);
        /// result = from(lst).avg<int>([](const int &i) { return i; });
        /// \endcode
        /// \todo Add autodeduction template args
        R result(0);
        foreach(TI item, _t)
        {
            result += func(item);
        }
        R calc = (_t.size()==0)?0:result/_t.size();
        return calc;
    }

    template<class R>
    typename std::enable_if<!std::is_pointer<R>::value && !std::is_arithmetic<R>::value, R >::type
    avg(const std::function<R(const TI &)> &func) const
    {
        /// \brief avg calculate the average of all the expression given by a lambda function
        /// \param func - const std::function<R(const TI &)> : lambda function returning a value. The average will be calculated from this result.
        /// \return R : Average of all the results of call of the the lambda function.
        /// \tparam T - optional/autodeducted - Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \tparam R : type of the value returned by func, and by the calculation of the average.
        /// \remarks This method is active only if R is not a pointer and is not arithmetic.
        /// \remarks R Must implement operator + and /
        /// \remarks R Must have a default constructor
        /// \code
        /// ...
        /// \endcode
        /// \todo example
        /// \todo Add autodeduction template args
        R result;
        if(_t.count()!=0)
        {
            foreach(TI item, _t)
            {
                result += func(item);
            }
            R calc = result/_t.count();
            return calc;
        }
        return result;
    }

    /// \brief avg calculate the average of all the expression given by a lambda function
    /// \param func Lambda function returning a R type. The average will be calculated from all the results.
    /// \returns A R Type average of all the results of call of the the lambda function.
    /// \remarks R must have a default constructor.
    template<class R>
    typename std::enable_if<std::is_pointer<R>::value, R>::type
    avg(const std::function<R(const TI &)> &func) const
    {
        typedef typename std::remove_pointer<R>::type RClass;
        R calc = nullptr;
        if(this->_t.count()!=0)
        {
            calc = new RClass();
            R result = new RClass ();
            foreach(TI item, _t)
            {
                *result += *func(item);
            }
            *calc = *result/_t.count();
            delete (result);
        }
        return calc;
    }

    /// \brief contains allows to check if an item is present in the current list.
    /// \param item : item searched
    /// \returns true if the item is present in the current list.
    /// \remarks TI must implement operator ==
    template <class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, bool >::type
    contains(const TI & item)
    {
        auto it = this->_t.begin();

        while(it!=this->_t.end())
        {
            if(item==*it)
                return true;
            it++;
        }
        return false;
    }

    /// \brief contains allows to check if an item is present in the current list.
    /// \param item : item searched
    /// \returns true if the item is present in the current list.
    /// \remarks TI must implement operator ==
    template <class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, bool >::type
    contains(const TI & item)
    {
        auto it = this->_t.begin();

        while(it!=this->_t.end())
        {
            if(*item==**it)
                return true;
            it++;
        }
        return false;
    }

    /// \brief cast a list items to another type
    /// \tparam R : result type of an item
    template<class R>
    QLinq<QLinqList<R>> cast()
    {
        QLinqList<R> lst = _cast<R>();

        QLinq<QLinqList<R>> result (lst);
        return result;
    }

    /// \brief Returns the size of the list.
    int count() const { return _t.size(); }

    int count(const std::function<bool(const TI &)> &func) const
    {
        /// \brief Returns the number of items who satisfies the condition func.
        /// \param func - const std::function<bool(const TI &)> & :
        /// \return int : A number of items.
        /// \tparam T - optional/autodeducted - Type Container Class (QList, ...)
        /// \tparam TI - optional/autodeducted : Type of items in Container List
        /// \code
        /// QList<int> lst;
        /// for (int i=0; i<10; i++) { lst.insert(lst.end(),i); }
        /// int result = from(lst).count([](const int &i) { return i%2==0; };
        /// \endcode
        /// \todo Add unit tests

        int lCount(0);
        foreach(const TI item, _t)
        {
            if(func(item)) lCount ++;
        }
        return lCount;
    }

    /// \brief Concatenation of 2 Lists
    /// \param data the objects of the list must be the same of which stored in the active QLinq list
    /// \returns A new QLinq with all the items concatenated
    QLinq<T> concat(const T & data) const
    {
        T tmpData = this->_concat(data);
        return QLinq(tmpData);
    }

    /// \brief Returns a new QLinq item with the default value if the current list is empty
    /// \param defaultValue : value to add the the list if the list is empty.
    /// \returns QLinq<T>
    QLinq<T> defaultIfEmpty(const TI & defaultValue )
    {
        if(_t.size()!=0)
        {
            return *this;
        }
        else
        {
            T lst;
            lst.append(defaultValue);
            return QLinq(lst);
        }
    }

    /// \brief Keeps only one value in the current QLinq List.
    /// \returns A new QLinq with the list cleaned
    /// \remarks TI must implement operator==
    inline QLinq<T> distinct()
    {
        T t = this->_distinct(this->_t);
        return (QLinq<T>(t));
    }

    /// \brief Remove a list of items from the current QLinq List.
    /// \param data A list of items to be remove from the current QLinq List
    /// \remarks TI must implement operator !=
    /// \remarks data must be a container of TI
    QLinq<T> except(const T & data) const
    {
        T tmpData = this->_except_lq(data);
        return QLinq<T>(tmpData);
    }

    /// \brief Returns the first item of the current QLinq List
    /// \remarks check before the list is empty or use firstOfDefault instead.
    TI first()
    {
        auto it = _t.begin();
        return *it;
    }

    /// \brief Returns the first item of the current QLinq List who matches the lambda function
    /// \remarks check before the list is empty or use firstOfDefault instead.
    inline typename T::value_type first(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        return *it;
    }

    /// \brief Returns the first item of the current QLinq List. If the list is empty, the return value will be 0.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && std::is_arithmetic<TI2>::value, TI>::type firstOrDefault() const
    {
        if(_t.size()==0)  return static_cast<TI2>(0);
        else return *(_t.begin());
    }

    /// \brief Returns the first item of the current QLinq List. If the list is empty, the return value will be an instance of TI.
    /// \remarks TI must have a default constructor.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && !std::is_arithmetic<TI2>::value, TI>::type firstOrDefault() const
    {
        if(_t.size()==0)  return TI2();
        else return *(_t.begin());
    }

    /// \brief Returns the first item of the current QLinq List. If the list is empty, the return value will nullptr.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<std::is_pointer<TI2>::value, TI>::type firstOrDefault() const
    {
        if(_t.size()==0)  return nullptr;
        else return *(_t.begin());
    }

    /// \brief Returns the first item of the current QLinq List who matches the lambda function. If no item matches, returns 0.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && std::is_arithmetic<TI2>::value, TI>::type
    firstOrDefault(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        if(it==_t.end())  return static_cast<TI2>(0);
        return *it;
    }

    /// \brief Returns the first item of the current QLinq List who matches the lambda function. If no item matches, returns a new instance of TI.
    /// \remarks TI must have a default constructor.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && !std::is_arithmetic<TI2>::value, TI>::type
    firstOrDefault(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        if(it==_t.end())  return TI2();
        return *it;
    }

    /// \brief Returns the first item of the current QLinq List who matches the lambda function. If no item matches, returns nullptr.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<std::is_pointer<TI2>::value, TI>::type
    firstOrDefault(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        if(it==_t.end())  return nullptr;
        return *it;
    }

    /// \brief Loops on all the items
    /// \param func Lambda function to process on one item
    /// \returns returns the active QLinq list
    QLinq<T> forEach(const std::function<void(const TI &)> &func) const
    {
        this->_forEach(func);
        return *this;
    }

    /// \brief Returns a QLinq with the result of a group by
    /// \tparam F Class type of a group line.
    /// \tparam R Class type of the result of the group by.
    /// \param funcFields : returns a line to be grouped.
    /// \param func : returns a line of the result.
    /// \remarks F must implement operator ==
    template <class F, class R> QLinq<QList<R>> groupBy(const std::function<F(const TI &)> funcFields,const std::function<R(const F&,const QLinq<T>&)> &func) const
    {
        QList<R> result = this->_groupBy(funcFields,func);
        return QLinq<QList<R>>(result);
    }

    /// \brief Returns a QLinq with the result of a group by.
    /// \brief a join will be made between the current list and the second to create the groupby.
    /// \tparam J Class type of the second item List.
    /// \tparam F return type of the join.
    /// \tparam R return type of the groupby.
    /// \remarks J must implement operator ==
    template <class J, class F, class R> QLinq<QList<R>>
    groupJoin(
            QList<J> &joinTable
    ,       const std::function<F(const TI &)> &funcPrimary
    ,       const std::function<F(const J&)> &funcForeign
    ,       const std::function<R(const TI&,const QLinq<QList<J>>&)> &functionSelect
    )
    {
        QList<R> resultList = this->_groupJoin(joinTable,funcPrimary,funcForeign,functionSelect);
        return QLinq<QList<R>>(resultList);
    }

    // TODO Modify groupJoin to allow all the type of lists
    /// \brief Returns a QLinq with the result of a group by for each row of TI.
    /// \tparam J Class type of the second item List
    /// \tparam R Class type of an item of Result
    /// \param joinTable : QList<J> list of item for grouping
    /// \param funcJoin : lambada function (const TI &, const J &)->bool. This function allow to define the join between both lists.
    /// \param functionSelect : lambda function (const TI&,const QLinq<QList<J>> &)->R : Function to create an item of result (R).
    /// \returns QLinq<QList<R>>
    template <class J, class R> QLinq<QList<R>>
    groupJoin(
            QList<J> &joinTable
    ,       const std::function<bool(const TI &,const J&)> &funcJoin
    ,       const std::function<R(const TI&,const QLinq<QList<J>>&)> &functionSelect
    )
    {
        QList<R> resultList = this->_groupJoin(joinTable, funcJoin ,functionSelect);
        return QLinq<QList<R>>(resultList);
    }

    /// \brief Returns a QLinq of a list of items belonging to the current list and to a second list.
    /// \param data : second list.
    /// Returns QLinq<T>
    QLinq<T> intersect(const T & data) const
    {
        T tmpData = this->_intersect(data);
        return QLinq(tmpData);
    }

    // TODO : Modify join to allow all type of List
    /// \brief Returns a QLinq of a list of the result of the join of the current list and joinTable.
    /// \tparam F : Type of the field to join.
    /// \tparam R : Class type of a item of result.
    /// \tparam LJ : Class type of the second list to join.
    /// \param joinTable : List of item to join.
    /// \param funcPrimary : Lambda function returning the key to join from the current list.
    /// \param funcPrimary : Lambda function returning the key to join from the second list.
    /// \param functionSelect : Lambda function returning an item of the join.
    template <class F, class R,class LJ, class J = typename LJ::value_type> QLinq<QList<R>>
    join(
            const LJ &joinTable
    ,       const std::function<F(const TI&)> &funcPrimary
    ,       const std::function<F(const typename LJ::value_type&)> &funcForeign
    ,       const std::function<R(TI &,typename LJ::value_type &)> &functionSelect
    )
    {
        QList<R> resultList = this->_join(joinTable, funcPrimary, funcForeign, functionSelect);
        return QLinq<QList<R>>(resultList);
    }

    /// \brief Returns a QLinq of a list of the result of the join of the current list and joinTable.
    /// \tparam J : Class type of the second list to join.
    /// \tparam R : Class type of a item of result.
    /// \param joinTable : List of item to join.
    /// \param funcJoin : Lambda function returning true if the 2 items allow the join.
    /// \param functionSelect : Lambda function returning an item of the join.
    template <class R,class LJ, class J = typename LJ::value_type > QLinq<QList<R>>
    join(
            const LJ &joinTable
    ,       const std::function<bool(const TI&, const typename LJ::value_type&)> &funcJoin
    ,       const std::function<R(TI &, typename LJ::value_type &)> &functionSelect
    )
    {
        QList<R> resultList = this->_join(joinTable, funcJoin, functionSelect);
        return QLinq<QList<R>>(resultList);
    }

    template <class J> QLinq<QList<std::tuple<TI, J>>>
    join(
            const QList<J> &joinTable
    ,       const std::function<bool(const TI&, const J&)> &funcJoin
    )
    {
        QList<std::tuple<TI, J>> resultList = this->_join(joinTable, funcJoin);
        return QLinq<QList<std::tuple<TI, J>>>(resultList);
    }

#if __IS_CPP_14__
    //template <class J, typename... Ts> QLinq<QList<std::tuple<Ts...>>>
    template <class J //, class = typename std::enable_if<std::tuple_size<TI>::value!=0>::type
              > //QLinq<QList<std::tuple<Ts..., J>>>
    auto
    joinTuple(
            const QList<J> &joinTable
    ,       const std::function<bool(const TI&, const J&)> &funcJoin)
    {
        typedef decltype (std::tuple_cat(this->_t.at(0),std::make_tuple(joinTable.at(0)))) tuple_cat_type;
        //QList<std::tuple<Ts..., J>> resultList = this->_joinTuple(joinTable, funcJoin);
        QList<tuple_cat_type> resultList = this->_joinTuple(joinTable, funcJoin);
        //return QLinq<QList<std::tuple<Ts..., J>>>(resultList);
        return QLinq<QList<tuple_cat_type>>(resultList);
    }
#endif

    /// \brief Returns the last item of the current QLinq List
    /// \remarks check before the list is empty or use lastOfDefault instead.
    TI last() const
    {
        auto it = _t.end() - 1;
        return *it;
    }

    /// \brief Returns the last item of the current QLinq List who matches the lambda function
    /// \remarks check before the list is empty or use lastOfDefault instead.
    inline typename T::value_type last(const std::function<bool(typename T::value_type)> &func)
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        auto it2 = it;
        while(it!=_t.end())
        {
            it2 = it;
            it = std::find_if(it + 1,_t.end(),func);
        }
        return *it2;
    }

    /// \brief Returns the last item of the current QLinq. returns 0 if the list is empty.
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && std::is_arithmetic<TI2>::value, TI>::type lastOrDefault() const
    {
        if(_t.size()==0)  return static_cast<TI2>(0);
        else return *(_t.end() -1 );
    }

    /// \brief Returns the last item of the current QLinq. returns a new TI if the list is empty.
    /// \remarks TI must have a default constructor
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && !std::is_arithmetic<TI2>::value, TI>::type lastOrDefault() const
    {
        if(_t.size()==0)  return TI2();
        else return *(_t.end() -1 );
    }

    /// \brief Returns the last item of the current QLinq. returns a new TI if the list is empty.
    /// \remarks TI must have a default constructor
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<std::is_pointer<TI2>::value, TI>::type lastOrDefault() const
    {
        if(_t.size()==0)  return nullptr;
        else return *(_t.end() -1 );
    }

    /// \brief Returns the last item of the current QLinq. returns a new TI if the list is empty.
    /// \remarks TI must have a default constructor
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && std::is_arithmetic<TI2>::value, TI>::type
    lastOrDefault(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);

        if(it==_t.end())  return static_cast<TI2>(0);

        auto it2 = it;
        while(it!=_t.end())
        {
            it2 = it;
            it = std::find_if(it + 1,_t.end(),func);
        }
        return *it2;
    }

    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value && !std::is_arithmetic<TI2>::value, TI>::type
    lastOrDefault(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        if(it==_t.end())  return TI2();
        auto it2 = std::find_if(it,_t.end(),func);
        while(it!=_t.end())
        {
            it2 = it;
            it = std::find_if(it + 1,_t.end(),func);
        }
        return *it2;
    }

    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<std::is_pointer<TI2>::value, TI>::type
    lastOrDefault(const std::function<bool(typename T::value_type)> &func) const
    {
        auto it = std::find_if(_t.begin(),_t.end(),func);
        if(it==_t.end())  return nullptr;
        auto it2 = std::find_if(it,_t.end(),func);
        while(it!=_t.end())
        {
            it2 = it;
            it = std::find_if(it + 1,_t.end(),func);
        }
        return *it2;
    }

    template<class T2 = T, class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, QLinq<T>>::type orderBy()
    {
        std::sort(this->_t.begin(), this->_t.end(), [](TI2 t1,TI2 t2) { return (t1<t2); });
        return *this;
    }

    template<class T2 = T, class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, QLinq<T>>::type orderBy()
    {
        std::sort(this->_t.begin(), this->_t.end(), [](TI2 t1,TI2 t2) { return (*t1<*t2); });
        return *this;
    }

    template <class R> QLinq<T> orderBy(const std::function<R(TI)> &func)
    {
        this->_orderBy(func,this->_t);
        return *this;
    }

    // TODO orderByDescending

    template<class T2 = T, class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, QLinq<T>>::type orderByDescending()
    {
        std::sort(this->_t.begin(), this->_t.end(), [](TI2 t1,TI2 t2) { return (t1>t2); });
        return *this;
    }

    template<class T2 = T, class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, QLinq<T>>::type orderByDescending()
    {
        std::sort(this->_t.begin(), this->_t.end(), [](TI2 t1,TI2 t2) { return (*t1>*t2); });
        return *this;
    }

    template <class R> QLinq<T> orderByDescending(const std::function<R(TI)> &func)
    {
        this->_orderByDescending(func,this->_t);
        return *this;
    }

    template <class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, TI2>::type
    max() const
    {
        Q_ASSERT_X(_t.size()!=0,"QLinq Max","Call max on an empty list");
        TI2 max = _t.at(0);
        foreach(TI2 item, _t)
        {
            if(max<item) max = item;
        }
        return max;
    }

    template <class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, TI2>::type
    max() const
    {
        Q_ASSERT_X(_t.size()!=0,"QLinq Max","Call max on an empty list");
        TI2 max = _t.at(0);
        foreach(TI2 item, _t)
        {
            if(*max<*item) max = item;
        }
        return max;
    }

//    template<typename O>
//    typename std::enable_if<!std::is_pointer<O>::value, O>::type
    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if<!std::is_pointer<R>::value && !std::is_member_pointer<LF>::value, typename std::remove_reference<R>::type>::type
    max(const LF func) const
    {
        typedef typename std::remove_reference<R>::type tmpResultType;
        typedef typename std::remove_const<tmpResultType>::type resultType;
        Q_ASSERT_X(_t.size()!=0,"QLinq Max","Call max on an empty list");
        resultType max = func(_t.first());

        foreach(TI item,_t)
        {
            resultType candidate = func(item);
            if(candidate>max)
            {
                max = candidate;
            }
        }
        return max;
    }

//    template<typename O>
//    typename std::enable_if<std::is_pointer<O>::value, O>::type
    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if<std::is_pointer<R>::value && !std::is_member_pointer<LF>::value, R>::type
    max(const LF func) const
    {
        typedef typename std::remove_reference<R>::type tmpResultType;
        typedef typename std::remove_const<tmpResultType>::type resultType;
        Q_ASSERT_X(_t.size()!=0,"QLinq Max","Call max on an empty list");
        resultType max = func(_t.first());

        foreach(TI item,_t)
        {
            resultType candidate = func(item);
            if(*candidate>*max)
            {
                max = candidate;
            }
        }
        return max;
    }

    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if<std::is_member_pointer<LF>::value, typename std::remove_reference<R>::type>::type
    max(const LF) const { return 0; }


    template<typename O> TI maxItem(const std::function<O(TI)> &func) const
    {
        O max = func(_t.first());
        TI Tmax = _t.first();

        foreach(TI item,_t)
        {
            O candidate = func(item);
            if(candidate>max)
            {
                Tmax = item;
                max = candidate;
            }
        }
        return Tmax;
    }

    template <class TI2 = TI>
    typename std::enable_if<!std::is_pointer<TI2>::value, TI2>::type
    min() const
    {
        Q_ASSERT_X(_t.size()!=0,"QLinq Min","Call min on an empty list");
        TI2 min = _t.at(0);
        foreach(TI2 item, _t)
        {
            if(min>item) min = item;
        }
        return min;
    }

    template <class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, TI2>::type
    min() const
    {
        Q_ASSERT_X(_t.size()!=0,"QLinq Min","Call min on an empty list");
        TI2 min = _t.at(0);
        foreach(TI2 item, _t)
        {
            if(*min>*item) min = item;
        }
        return min;
    }

//    template<typename O>
//    typename std::enable_if<!std::is_pointer<O>::value, O>::type
    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if<!std::is_pointer<R>::value && !std::is_member_pointer<LF>::value, typename std::remove_reference<R>::type>::type
    min(const LF func) const
    {
        typedef typename std::remove_reference<R>::type tmpResultType;
        typedef typename std::remove_const<tmpResultType>::type resultType;
        Q_ASSERT_X(_t.size()!=0,"QLinq Min","Call min on an empty list");
        resultType min = func(_t.first());

        foreach(TI item,_t)
        {
            resultType candidate = func(item);
            if(candidate<min)
            {
                min = candidate;
            }
        }
        return min;
    }

//    template<typename O>
//    typename std::enable_if<std::is_pointer<O>::value, O>::type
    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if<std::is_pointer<R>::value && !std::is_member_pointer<LF>::value, R>::type
    min(const LF func) const
    {
        typedef typename std::remove_reference<R>::type tmpResultType;
        typedef typename std::remove_const<tmpResultType>::type resultType;
        Q_ASSERT_X(_t.size()!=0,"QLinq Max","Call min on an empty list");
        resultType min = func(_t.first());

        foreach(TI item,_t)
        {
            resultType candidate = func(item);
            if(*candidate<*min)
            {
                min = candidate;
            }
        }
        return min;
    }

    template<typename O> TI minItem(const std::function<O(TI)> &func) const
    {
        O min = func(_t.first());
        TI Tmin = _t.first();

        foreach(TI item,_t)
        {
            O candidate = func(item);
            if(candidate<min)
            {
                Tmin = item;
                min = candidate;
            }
        }
        return Tmin;
    }

    /// \brief Create a QLinq object filled with incremented value
    /// \param start : first value
    /// \param count : number a value wanted.
    /// \return a qLinq with ( start ), ( start + 1 ),... ( start + count - 1 ).
    static QLinq<T> range(TI start, int count)
    {
        T lst;

        for(int i=0; i<count; i++)
            lst.append(start + i);

        return QLinq<T>(lst);
    }

    static QLinq<T> repeat(const TI & item, int nb)
    {
        T lst;
        for(int i=0; i<nb; i++)
            lst.append(item);

        return QLinq<T>(lst);
    }

    /// \brief Reverse all the items in the current list.
    QLinq<QList<TI>> reverse()
    {
        return QLinq<TI>(_reverse());
    }

    //template <typename R> R sum(const std::function<R(const TI &)> &func) const
//    template <typename LF,typename R=typename std::result_of<LF(const TI &)>::type,typename std::enable_if<!std::is_member_pointer<LF>::value,LF::type>::type >
//    auto
//    sum(const LF func) const ->typename std::remove_reference<R>::type
    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if< !std::is_member_pointer<LF>::value, typename std::remove_reference<R>::type>::type
    sum(const LF func) const
    {
        typedef typename std::remove_reference<R>::type tmpResultType;
        typedef typename std::remove_const<tmpResultType>::type resultType;
        resultType result=0;
        foreach(TI item,_t)
        {
            result += func(item);
        }
        return result;
    }

    template<typename LF,typename R=typename std::result_of<LF(const TI &)>::type>
    typename std::enable_if< std::is_member_pointer<LF>::value, typename std::remove_reference<R>::type>::type
    sum(const LF) const
    {
        typedef typename std::remove_reference<R>::type tmpResultType;
        typedef typename std::remove_const<tmpResultType>::type resultType;
        resultType result = 0;
        return result ;
    }

    template <class R> QLinq<QList<R>> select(const std::function<R(TI)> &func)
    {
        QList<R> result = this->_select(func);
        QLinq<QList<R>> myLinq(result);
        return myLinq;
    }

    template <typename R, typename J> QLinq<QList<R>> selectMany(J joinTable, const std::function<R(const TI &,const typename J::value_type &)> &functionSelect)
    {
        QList<R> lstResult = this->_selectMany(joinTable,functionSelect);
        return QLinq<QList<R>>(lstResult);
    }

    ///
    /// \brief sequenceEquals
    /// \param data
    /// \return
    /// \remarks if T is a class, it must implement operator==
    template<class TI2 = TI>
    typename std::enable_if<std::is_arithmetic<TI2>::value || !std::is_pointer<TI2>::value, bool>::type sequenceEquals(T data) const
    {
        if(_t.size()!=data.size())
        {
            return false;
        }
        auto tIt = _t.begin();
        auto dataIt = data.begin();
        while(tIt!=_t.end())
        {
            if(*tIt!=*dataIt)
            {
                return false;
            }
            tIt++;
            dataIt++;
        }
        return true;
    }

    // TODO Example sequence
    // TODO Doc sequence
    static QLinq<T> sequence(const TI & start, int count, const std::function<TI(int pos, TI prec)> &func)
    {
        T lst;
        TI prec = start;
        for( int i=0; i<count; i++ )
        {
            lst.append( prec );
            prec = func( i, prec );
        }
        return QLinq<T>(lst);
    }

    // TODO Example sequence
    // TODO Doc sequence
    static QLinq<T> sequence(TI start, int count, const std::function<TI(const QLinq<T> &prec)> &func)
    {
        T lst;
        TI prec = start;
        for( int i=0; i<count; i++ )
        {
            lst.append( prec );
            prec = func( QLinq<T>(lst) );
        }
        return QLinq<T>(lst);
    }

    // TODO Comment
    template<class TI2 = TI>
    typename std::enable_if<!std::is_arithmetic<TI2>::value && std::is_pointer<TI2>::value, bool>::type sequenceEquals(T data) const
    {
        if(_t.count()!=data.count())
        {
            return false;
        }
        for(int i=0;i<_t.count();i++)
        {
            if(*_t[i]!=*data[i])
            {
                return false;
            }
        }
        return true;
    }

    // TODO Single

    // TODO SingleOrDefault

    QLinq<T> skip(int nb)
    {
        T lst = this->_skip((nb));
        return QLinq(lst);
    }

    // TODO SkipWhile Comment
    QLinq<T> skipWhile(const std::function<bool(const TI &)> &func)
    {
        T lst = this->_skipWhile(func);
        return QLinq(lst);
    }

    ///
    /// \brief sum all the items in the list. The object within must implement operator +.
    /// \return the sum of all items.
    ///
    template<class TI2 = TI>
    typename std::enable_if<std::is_arithmetic<TI2>::value && !std::is_pointer<TI2>::value, TI2>::type
    sum() const
    {
        TI2 result=0;
        foreach(TI2 item,_t)
        {
            result += item;
        }
        return result;
    }

    ///
    /// \brief sum all the items in the list. The object within must implement operator +.
    /// \return the sum of all items.
    ///
    template<class TI2 = TI>
    typename std::enable_if<!std::is_arithmetic<TI2>::value && !std::is_pointer<TI2>::value, TI2>::type
    sum() const
    {
        TI2 result;
        foreach(TI2 item,_t)
        {
            result += item;
        }
        return result;
    }

    ///
    /// \brief sum all the items in the list. The object within must implement operator +.
    /// \return the sum of all items.
    /// \remarks Don't forget to delete the result
    template<class TI2 = TI>
    typename std::enable_if<std::is_pointer<TI2>::value, TI2>::type
    sum() const
    {
        typedef typename std::remove_pointer<TI2>::type RClass;
        TI2 result = new RClass();
        foreach(TI2 item,_t)
        {
            *result += *item;
        }
        return result;
    }

    /// \brief get the first items from a list.
    /// \param nb : number of items to take from the list
    /// \return the first items asked.
    ///  \remarks if nb is greater than the number of item, the full list will be returned
    QLinq<T> take(int nb)
    {
        if(nb>=this->count()) return *this;

        T lst = this->_take(nb);
        return QLinq(lst);
    }

    /// \brief get the first items from a list while the condition is true.
    /// \param funct : condition tested on all items until it returns false. func has one arguement of the type of the item of the list and returns a boolean.
    /// \return the first items asked.
    QLinq<T> takeWhile(const std::function<bool(const TI &)> &func)
    {
        T lst = this->_takeWhile(func);
        return QLinq(lst);
    }

    // TODO ThenBy
    // TODO ThenByDescending

    /// \brief Convert the current list to a QHash List
    /// \param func : Lambda function to calculate a key from an item;
    /// \returns QHash List of the items
    template<class K>
    QHash<K,TI> toQHash(const std::function<K(const TI &)> &func)
    {
        QMap<K,TI> result;
        for(auto t = _t.cbegin(); *t !=_t.cend(); t++ )
        {
            K key = func(*t);
            result.insert(key, *t);
        }
        return result;
    }

    /// \brief Convert the current list to a QMap List
    /// \param func : Lambda function to calculate a key from an item;
    /// \returns QMap List of the items
    template<class K>
    QMap<K,TI> toQMap(const std::function<K(const TI &)> &func)
    {
        QMap<K,TI> result;
        for(auto t = _t.cbegin(); *t !=_t.cend(); t++ )
        {
            K key = func(*t);
            result.insert(key, *t);
        }
        return result;
    }

    /// \brief Convert the current list to a QMultiHash List
    /// \param func : Lambda function to calculate a key from an item;
    /// \returns QMultiHash List of the items
    template<class K>
    QMultiHash<K,TI> toQMultiHash(const std::function<K(const TI &)> &func)
    {
        QMultiHash<K,TI> result;
        for(auto t = _t.cbegin(); *t !=_t.cend(); t++ )
        {
            K key = func(*t);
            result.insert(key, *t);
        }
        return result;
    }

    /// \brief Convert the current list to a QMultiMap List
    /// \param func : Lambda function to calculate a key from an item;
    /// \returns QMultiMap List of the items
    template<class K>
    QMultiMap<K,TI> toQMultiMap(const std::function<K(const TI &)> &func)
    {
        QMultiMap<K,TI> result;
        for(auto t = _t.cbegin(); *t !=_t.cend(); t++ )
        {
            K key = func(*t);
            result.insert(key, *t);
        }
        return result;
    }

    /// \brief Convert the current list to a QLinkedList
    /// \returns QLinkedList of the items
    inline QLinkedList<TI> toQLinkedList()
    {
        QLinkedList<TI> lst;
        foreach(auto item,_t)
        {
            lst.append(item);
        }

        return lst;
    }

    /// \brief convert QLinq list to a QList.
    /// \return QList with all the items from the current list.
    inline QList<TI> toQList() const
    {
        QList<TI> lst;
        foreach(auto item,_t)
        {
            lst.append(item);
        }

        return lst;
    }

    /// \brief convert QLinq list to a QVector.
    /// \return QVector with all the items from the current list.
    inline QVector<TI> toQVector() const
    {
        QVector<TI> lst;
        foreach(auto item,_t)
        {
            lst.append(item);
        }

        return lst;
    }

    // TODO Comment
    QLinq<T> unionJoin(const T & data) const
    {
        T tmpData = _unionJoin(data);
        return QLinq(tmpData);
    }

    // TODO Comment
    // TODO Unit Test
    inline QLinq<T> where(const std::function<bool(const TI &)> &func)
    {
        QList<TI> myList = this->_where(func);
        QLinq<T> myLinq(myList);
        return myLinq;
    }

    // TODO Comment
    // TODO Unit Test
    inline QLinq<T> whereIf(bool condition, const std::function<bool(const TI &)> &func)
    {
        if(condition)
            return where(func);
        else
            return *this;
    }

    /// \brief Associaltes items from a List with another One.
    /// \param lst : second list with items to associate
    /// \param funct : lambda function to associate an item from the current list with a item from lst.
    /// \remarks if lst has more items them the active list, these items will be lost.
    /// \remarks if the current list has more items than lst, these items will be lost.
    /// \return The result will be a QLinq with the result of func on all items.
    template<class R, class O, typename OI = typename O::value_type >
    QLinq<QList<R>> zip(const O &lst,const std::function<R(const TI &elt1, const typename O::value_type &elt2)> &func)
    {
        auto result = _zip(lst, func);
        return QLinq<QList<R>>(result);
    }

protected:
    T _concat(const T & data) const
    {
        T tmpData;
        foreach (TI item, _t)
        {
            tmpData.append(item);
        }
        foreach (TI item, data)
        {
            tmpData.append(item);
        }
        return tmpData;
    }

protected:
    template<class R>
    QList<R> _cast()
    {
        QList<R> lst;

        auto it = this->_t.begin();
        while(it!=this->_t.end())
        {
            lst.append(static_cast<R>(*it));
            it++;
        }
        return lst;
    }


    template<class T2 = T, class TI2 = TI> inline typename std::enable_if<!std::is_pointer<TI2>::value, T>::type _distinct(T lst)
    {
        T2 lst2(lst);

        std::sort(lst2.begin(), lst2.end());
        auto it = std::unique(lst2.begin(),lst2.end());
        lst2.erase(it,lst2.end());
        return lst2;
    }

    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<std::is_pointer<TI2>::value, T>::type _distinct(T lst)
    {
        T2 lst2(lst);

        std::sort(lst2.begin(), lst2.end(),[](const TI2 &t1, const TI2 &t2 ) { return *t1<*t2; });
        auto it = std::unique(lst2.begin(),lst2.end(),
                                  [](const TI2 &t1, const TI2 &t2 ) { return *t1==*t2; }
                                  );
        lst2.erase(it,lst2.end());
        return lst2;
    }

//    T _except(const T & data) const
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<!std::is_pointer<TI2>::value, T>::type _except_lq(const T & data) const
    {
        T tmpData;
        foreach (TI item, _t)
        {
            if(!data.contains(item))
            {
                tmpData.append(item);
            }
        }
        return tmpData;
    }

    //    T _except(const T & data) const
    template<class T2 = T, class TI2 = TI> inline
    typename std::enable_if<std::is_pointer<TI2>::value, T>::type _except_lq(const T & data) const
    {
        T2 tmpData;
        foreach (TI item, _t)
        {
            bool test = false;

            auto it = data.begin();
            while(!test && it!=data.end())
            {
                test = (**it==*item);
                it++;
            }
            if(!test)
                tmpData.append(item);
        }
        return tmpData;
    }

    void _forEach(const std::function<void(const TI &)> &func) const
    {
        auto it = _t.begin();
        while (it!=_t.end())
        {
            func(*it);
            it ++;
        }
    }

    template <class F, class R, class QLQT> QList<R> _groupBy(const std::function<F(const TI &)> funcFields,const std::function<R(const F&,QLQT)> &func) const
    {
        // Build the dictionnary
        QHash<F,QList<TI>> dictionnary;
        foreach(auto item, _t)
        {
            F key = funcFields(item);
            if(dictionnary.contains(key))
            {
                dictionnary[key].append(item);
            }
            else
            {
                dictionnary[key] = QList<TI>();
                dictionnary[key].append(item);
            }
        }

        // Execute the statements;
        QList<R> result;
        foreach(auto key, dictionnary.keys())
        {
            result.append( func(key, QLinq(dictionnary[key])) );
        }
        return result;
    }

    template <class J, class F, class R, class QLQ> QList<R>
    _groupJoin(
        const QList<J> &joinTable
    ,   const std::function<F(const TI &)> &funcPrimary
    ,   const std::function<F(const J &)> &funcForeign
    ,   const std::function<R(const TI&,const QLQ &)> &functionSelect )
    {
        QList<R> resultList;
        foreach(auto primary,this->_t)
        {
            QList<J> groupList;
            foreach(auto foreign,joinTable)
            {
                if(funcPrimary(primary)==funcForeign(foreign))
                {
                    //R result = functionSelect(primary,foreign);
                    groupList.append(foreign);
                }
            }
            if(groupList.count()!=0)
            {
                R result = functionSelect(primary,QLQ(groupList));
                resultList.append(result);
            }
        }
        return resultList;
    }

    template <class J, class R, class QLQ> QList<R>
    _groupJoin(
        const QList<J> &joinTable
    ,   const std::function<bool(const TI &,const J &)> &funcJoin
    ,   const std::function<R(const TI&,const QLQ &)> &functionSelect )
    {
        QList<R> resultList;
        foreach(auto primary,this->_t)
        {
            QList<J> groupList;
            foreach(auto foreign,joinTable)
            {
                if(funcJoin(primary,foreign))
                {
                    //R result = functionSelect(primary,foreign);
                    groupList.append(foreign);
                }
            }
            if(groupList.count()!=0)
            {
                R result = functionSelect(primary,QLQ(groupList));
                resultList.append(result);
            }
        }
        return resultList;
    }

    T _intersect(const T & data) const
    {
        T tmpData;
        foreach (TI item, _t)
        {
            foreach (TI item2, data)
            {
                if(item==item2)
                {
                    if(!tmpData.contains(item))
                    {
                        tmpData.append(item);
                    }
                }
            }
        }
        return tmpData;
    }

    template <class J, class F, class R> QList<R>
    _join(
            const QList<J> &joinTable
    ,       const std::function<F(const TI&)> &funcPrimary
    ,       const std::function<F(const J &)> &funcForeign
    ,       const std::function<R(TI &, J&)> &functionSelect
    )
    {
        QList<R> resultList;
        foreach(auto primary,this->_t)
        {
            foreach(auto foreign,joinTable)
            {
                if(funcPrimary(primary)==funcForeign(foreign))
                {
                    R result = functionSelect(primary,foreign);
                    resultList.append(result);
                }
            }
        }
        return resultList;
    }

    template <class J, class R> QList<R>
    _join(
            const QList<J> &joinTable
    ,       const std::function<bool(const TI&, const J&)> &funcJoin
    ,       const std::function<R(TI &,J &)> &functionSelect
    )
    {
        QList<R> resultList;
        foreach(auto primary,this->_t)
        {
            foreach(auto foreign,joinTable)
            {
                if(funcJoin(primary,foreign))
                {
                    R result = functionSelect(primary,foreign);
                    resultList.append(result);
                }
            }
        }
        return resultList;
    }

    template <class J> QList<std::tuple<TI, J>>
    _join(
            const QList<J> &joinTable
    ,       const std::function<bool(const TI&, const J&)> &funcJoin
    )
    {
        QList<std::tuple<TI, J>> resultList;
        foreach(auto primary,this->_t)
        {
            foreach(auto foreign,joinTable)
            {
                if(funcJoin(primary,foreign))
                {
                    std::tuple<TI, J> result = std::make_tuple(primary, foreign);
                    resultList.append(result);
                }
            }
        }
        return resultList;
    }

    template <class J, class K, class R> QList<R>
    _join(
            const QList<J> &joinTable
    ,       IQLinqJoiner<TI, J, K, R> &joiner
    )
    {
        QList<R> resultList;
        if(joiner.isFullScan())
        {
            foreach(auto primary, this->_t)
            {
                foreach(auto foreign, joinTable)
                {
                    if( joiner.compare(primary,foreign) )
                    {
                        R result = joiner.join(primary, foreign);
                        resultList.append(result);
                    }
                }
            }
        }
        else
        {
            QMultiHash<K, R> dictionary;
            foreach(auto primary, this->_t)
            {
                dictionary.insert(joiner.getKeyA(primary), primary);
            }

            foreach(auto foreign, joinTable)
            {
                foreach(auto primary, dictionary.values(joiner.getKeyB(foreign)))
                {
                    R result = joiner.join(primary, foreign);
                    resultList.append(result);
                }
            }
        }
        return resultList;
    }

#if __IS_CPP_14__
    template <class J, typename ... Ts> //QList<std::tuple<Ts...,J>>
    auto
    _joinTuple(
            const QList<J> &joinTable
    ,       const std::function<bool(const TI&, const J&)> &funcJoin
    )
    const
    {
        typedef decltype (std::tuple_cat(this->_t.at(0),std::make_tuple(joinTable.at(0)))) tuple_cat_type;
        //QList<std::tuple<Ts..., J>> resultList;
        QList<tuple_cat_type> resultList;
        foreach(auto primary,this->_t)
        {
            foreach(J foreign,joinTable)
            {
                if(funcJoin(primary,foreign))
                {
                    auto val = std::make_tuple(foreign);
                    auto resultTmp = std::tuple_cat(primary, val);
                    //std::tuple<Ts...,J> &result = resultTmp;
                    //std::tuple<Ts...,J > result = (std::tuple<Ts...,J >) resultTmp;
                    resultList.append(resultTmp);
                }
            }
        }
        return resultList;
    }

    template <class J, class F, class TPL> //QList<std::tuple<Ts...,J>>
    auto
    _joinTuple(
            const QList<J> &joinTable
    ,       const std::function<F(const J &)> &funcForeign
    ,       const std::function<F(const TPL&)> &funcPrimary
    )
    const
    {
        typedef decltype (std::tuple_cat(this->_t.at(0),std::make_tuple(joinTable.at(0)))) tuple_cat_type;
        //QList<std::tuple<Ts..., J>> resultList;
        QList<tuple_cat_type> resultList;
        foreach(auto primary,this->_t)
        {
            foreach(J foreign,joinTable)
            {
                if(funcForeign(foreign)==funcPrimary(std::get<TPL>(primary)))
                {
                    auto val = std::make_tuple(foreign);
                    auto resultTmp = std::tuple_cat(primary, val);
                    //std::tuple<Ts...,J> &result = resultTmp;
                    //std::tuple<Ts...,J > result = (std::tuple<Ts...,J >) resultTmp;
                    resultList.append(resultTmp);
                }
            }
        }
        return resultList;
    }
#endif
    template <class R> void _orderBy(const std::function<R(TI)> &func,T &lst)
    {
        std::sort(lst.begin(), lst.end(), [func](TI t1,TI t2) { return (func(t1)<func(t2)); });
    }

    void _orderBy(const std::function<bool(TI,TI)> &func,T &lst)
    {
        std::sort(lst.begin(), lst.end(), func);
    }

    template <class R> void _orderByDescending(const std::function<R(TI)> &func,T &lst)
    {
        std::sort(lst.begin(), lst.end(), [func](TI t1,TI t2) { return (func(t1)>func(t2)); });
    }

    QList<TI> _reverse()
    {
        QList<TI> result;
        for(auto *it = this->_t.begin(); it++; it!=_t.end())
        {
            result.insert(0,*it);
        }
        return result;
    }

    template <class R> QList<R> _select(const std::function<R(TI)> &func)
    {
        QList<R> result;
        foreach(TI item, _t)
        {
            result.append(func(item));
        }
        return result;
    }

    template <typename J, typename R> QList<R> _selectMany(J joinTable, const std::function<R(const TI &,const typename J::value_type &)> &functionSelect)
    {
        QList<R> lstResult;

        foreach(auto item, _t)
        {
            foreach(auto itemJoin, joinTable)
            {
                lstResult.append(functionSelect(item,itemJoin));
            }
        }
        return lstResult;
    }

    T _skip(int nb)
    {
        T lst;

        if(nb<_t.count())
        {
            auto it=this->_t.begin()+nb;
            while(it!=this->_t.end())
            {
                lst.append(*it);
                it++;
            }
        }
        return lst;
    }

    T _skipWhile(const std::function<bool(const TI &)> &func)
    {
        T lst;
        bool copy = false;

        auto it=this->_t.begin();
        while(it!=this->_t.end())
        {
            if(!func(*it)) copy = true;
            if(copy) lst.append(*it);
            it++;
        }
        return lst;
    }

    T _take(int nb)
    {
        T lst;

        auto it=this->_t.begin();
        for(int i=0;i<nb;i++)
        {
            if(it!=_t.end())
            {
                lst.append(*it);
                it++;
            }
        }

        return lst;
    }

    T _takeWhile(const std::function<bool(const TI &)> &func)
    {
        T lst;

        auto it=this->_t.begin();
        while(it!=_t.end() && func(*it))
        {
            if(it!=_t.end())
            {
                lst.append(*it);
                it++;
            }
        }

        return lst;
    }

    T _unionJoin(const T & data) const
    {
        T tmpData;
        foreach (TI item, _t)
        {
            if(!tmpData.contains(item))
            {
                tmpData.append(item);
            }
        }
        foreach (TI item, data)
        {
            if(!tmpData.contains(item))
            {
                tmpData.append(item);
            }
        }
        return tmpData;
    }

    inline QList<TI> _where(const std::function<bool(TI)> &func)
    {
        QList<TI> myList;
        foreach(TI item,_t)
        {
            if(func(item)) { myList.append(item); }
        }
        return myList;
    }
//    template <typename V> inline QLinq<T> whereEquals(V(TI::* pm)(),const V &val)
//    {
//        QList<TI> myList;
//        foreach(TI item,this->_t)
//        {
//            std::function<V()> memberFunction =  std::bind(pm,item);
//            if(memberFunction()==val) { myList.append(item); }
//        }
//        QLinq<T> myLinq(myList);
//        return myLinq;
//    }

//    template <typename V> inline QLinq<T> whereEquals(V(TI::* pm)(),const V &val)
//    {
//        QList<TI> myList;
//        foreach(TI item,this->_t)
//        {
//            std::function<V()> memberFunction =  std::bind(pm,item);
//            if(memberFunction()==val) { myList.append(item); }
//        }
//        QLinq<T> myLinq(myList);
//        return myLinq;
//    }
    template<class R, class O, typename OI = typename O::value_type >
    QList<R> _zip(const O &lst,const std::function<R(const TI &elt1, const typename O::value_type &elt2)> &func)
    {
        QList<R> result;

        int minCount (std::min( _t.count(), lst.count()));

        for(int i=0; i<minCount; i++)
            result << func((*this)[i], lst[i]);

        return (result);
    }

protected:
    T _t;
};

#ifndef QLINQ2Q_H
template <class T> QLinq<T> from(T & myList)
{
    QLinq<T> myQLinq(myList);
    return myQLinq;
}
#endif // QLINQ2Q_H

#endif // QLINQ_H
