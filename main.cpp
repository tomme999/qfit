#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include <MQVM/twowaysbinding.h>
#include <MQVM/randomnumbergenerator.h>

#include "models/countdownmodel.h"
#include "viewModel/countdownviewmodel.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    CountDownViewModel data;
    data.setMinutes(5);
    data.setSeconds(10);


    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("ViewModel", &data);

    qmlRegisterType<TwoWaysBinding>("MQVM", 1, 0, "TwoWaysBinding");
    //qmlRegisterType<RandomNumberGenerator>("MQVM", 1, 0, "RandomNumberGenerator");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    //engine.findChild(""


    return app.exec();
}
