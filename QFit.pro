TEMPLATE = app

QT += qml quick
CONFIG += c++14
QMAKE_CXXFLAGS += -std=c++1y

SOURCES += main.cpp \
    models/countdownmodel.cpp \
    viewModel/countdownviewmodel.cpp \
    MQVM/mqvmhelper.cpp \
    MQVM/randomnumbergenerator.cpp \
    MQVM/twowaysbinding.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    models/countdownmodel.h \
    viewModel/countdownviewmodel.h \
    QLinq/qlinq.h \
    QLinq/qlinq2q.h \
    QLinq/qlinq2t.h \
    MQVM/mqvmhelper.h \
    MQVM/randomnumbergenerator.h \
    MQVM/twowaysbinding.h
