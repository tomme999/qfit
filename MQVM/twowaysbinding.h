#ifndef TWOWAYSBINDING_H
#define TWOWAYSBINDING_H

#include <QDebug>
#include <QObject>
#include <QQmlProperty>
#include <QQmlPropertyValueSource>


class TwoWaysBinding : public QObject, public QQmlPropertyValueSource
{
    Q_OBJECT
    Q_INTERFACES(QQmlPropertyValueSource)

    Q_PROPERTY(QObject* viewModel READ viewModel WRITE setViewModel NOTIFY viewModelChanged)
    Q_PROPERTY(QString attribute READ attribute WRITE setAttribute NOTIFY attributeChanged)

public:
    TwoWaysBinding(QObject *parent = nullptr)
        : QObject(parent), _viewModel(nullptr), _attribute("")
    {
//        qsrand(QDateTime::currentDateTime().toTime_t());
//        QObject::connect(&m_timer, SIGNAL(timeout()), SLOT(updateProperty()));
//        m_timer.start(500);
    }


    QString attribute() const { return this->_attribute; }
    void setAttribute(QString attribute) { this->_attribute = attribute; this->initialize(); }

    QObject * viewModel() const { return this->_viewModel; }
    void setViewModel( QObject * viewModel) { this->_viewModel = viewModel; this->initialize(); }

    virtual void setTarget(const QQmlProperty &prop) { m_targetProperty = prop; }

signals:
    void viewModelChanged();
    void attributeChanged();

private slots:
    void onAttributeChanged() {}

protected:
    void initialize()
    {
        if (_attribute!=nullptr && _viewModel!=nullptr && _attribute!=nullptr)
        {
            for(int i=0;i<_viewModel->metaObject()->propertyCount();i++)
            {
                qDebug() << i << ";" << _viewModel->metaObject()->property(i).name();
                if(_viewModel->metaObject()->property(i).name()==_attribute)
                {
                    QVariant val = _viewModel->metaObject()->property(i).read(_viewModel);
                    qDebug() << val;
                    qDebug() << m_targetProperty.write(10);
                    qDebug() << m_targetProperty.write(val.toInt());
                    m_targetProperty.connectNotifySignal(this,SLOT(onAttributeChanged));
                    //m_targetProperty.ty
                }
            }
        }
    }

private:
    QQmlProperty m_targetProperty;

    QObject * _viewModel;
    QString _attribute;

};

#endif // TWOWAYSBINDING_H
