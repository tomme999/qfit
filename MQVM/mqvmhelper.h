#ifndef MQVMHELPER_H
#define MQVMHELPER_H

#include <QObject>
// See QMetaObject::connectSlotsByName ( this )
// See http://doc.qt.io/qt-4.8/qtbinding.html
// Connect onViewModel<SignalName> to SignalName (QML => C++)
// Connect onQuick<SignalName> to SignalName (C++ => QML)
class MQVMHelper : public QObject
{
    Q_OBJECT
public:
    explicit MQVMHelper(QObject *parent = 0);

signals:

public slots:
};

#endif // MQVMHELPER_H
