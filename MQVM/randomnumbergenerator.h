#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H


// Source http://doc.qt.io/qt-5/qtqml-cppintegration-definetypes.html
#include <QDateTime>
#include <QObject>
#include <QQmlProperty>
#include <QQmlPropertyValueSource>
#include <QTimer>

class RandomNumberGenerator : public QObject, public QQmlPropertyValueSource
{
    Q_OBJECT
    Q_INTERFACES(QQmlPropertyValueSource)
    Q_PROPERTY(int maxValue READ maxValue WRITE setMaxValue NOTIFY maxValueChanged)
public:
    RandomNumberGenerator(QObject *parent = nullptr)
        : QObject(parent), m_maxValue(100)
    {
        qsrand(QDateTime::currentDateTime().toTime_t());
        QObject::connect(&m_timer, SIGNAL(timeout()), SLOT(updateProperty()));
        m_timer.start(500);
    }

    int maxValue() const { return m_maxValue;}
    void setMaxValue(int maxValue) {m_maxValue = maxValue;}

    virtual void setTarget(const QQmlProperty &prop) { m_targetProperty = prop; }



signals:
    void maxValueChanged();

private slots:
    void updateProperty() {
        m_targetProperty.write(qrand() % m_maxValue);
    }

private:
    QQmlProperty m_targetProperty;
    QTimer m_timer;
    int m_maxValue;
};
#endif // RANDOMNUMBERGENERATOR_H
