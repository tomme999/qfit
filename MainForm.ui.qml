import QtQuick 2.6
import MQVM 1.0

Rectangle {
    property alias mouseArea: mouseArea
    property alias myText: myText

    width: 360
    height: 360

    FontLoader { id: localFont; source: "qrc:/fonts/digital-7/digital-7.ttf" }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Text {
        id: myText
        //text: ViewModel.seconds
        TwoWaysBinding on text { viewModel:ViewModel; attribute: "seconds"}
        //RandomNumberGenerator on text {maxValue: 10;}
        anchors.centerIn: parent
        /*"00:00.000"*/

        font { family: localFont.name; pixelSize: 30}

    }
}
