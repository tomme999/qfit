#include "countdownmodel.h"

CountDownModel::CountDownModel(QObject *parent) : QObject(parent)
{

}

short CountDownModel::getMinutes() const
{
    return _minutes;
}

void CountDownModel::setMinutes(short minutes)
{
    _minutes = minutes;
    this->minutesChanged();
}

short CountDownModel::getSeconds() const
{
    return _seconds;
}

void CountDownModel::setSeconds(short seconds)
{
    _seconds = seconds;
}
