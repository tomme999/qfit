#ifndef COUNTDOWNMODEL_H
#define COUNTDOWNMODEL_H

#include <QObject>

class CountDownModel : public QObject
{
    Q_OBJECT

public:
    explicit CountDownModel(QObject *parent = 0);

    Q_PROPERTY(short minutes MEMBER _minutes READ getMinutes WRITE setMinutes NOTIFY minutesChanged)
    Q_PROPERTY(short seconds MEMBER _seconds READ getSeconds WRITE setSeconds NOTIFY secondsChanged)

    short getMinutes() const;
    void setMinutes(short getMinutes);

    short getSeconds() const;
    void setSeconds(short seconds);

private:
    short   _minutes;
    short   _seconds;

signals:
    void minutesChanged();
    void secondsChanged();

public slots:

};

#endif // COUNTDOWNMODEL_H
